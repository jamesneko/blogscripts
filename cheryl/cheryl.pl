#!/usr/bin/perl
#
# Inspired from
# http://jxf.me/entries/cheryls-birthday/
# and 
# https://github.com/mad4j/puzzles/blob/master/src/dolmisani/puzzles/CherylBirthday.java
#
# Cheryl's Birthday problem.
# 
# Albert and Bernard have just met Cheryl. 
# "When is your birthday?" Albert asked Cheryl. 
# Cheryl thought for a moment and said, "I won't tell you, but I'll give you some clues".
# She wrote down a list of ten dates:
#  * May 15, May 16, May 19
#  * June 17, June 18
#  * July 14, July 16
#  * August 14, August 15, August 17
#  
#  "One of these is my birthday" she said.
#  Cheryl whispered in Albert's ear the month, and only the month, of her birthday. 
#  To Bernard, she whispered the day, and only the day. 
#  "Can you figure it out now?" she asked Albert.
#  
#  Albert: "I don't know when your birthday is, but I know Bernard doesn't know, either."
#  
#  Bernard: "I didn't know originally, but now I do."
#  
#  Albert: "Well, now I know, too!"
#  
#  When is Cheryl's birthday?

use strict;
use warnings;
use 5.010;
use Term::ANSIColor;

# Map our dates into a nice list of hashes structure
my @DATES = qw/
		May_15 May_16 May_19
		June_17 June_18
		July_14 July_16
		August_14 August_15 August_17
	/;
@DATES = map { my ($m, $d) = split /_/; { month => $m, day => $d } } @DATES;

# And back to strings so we can follow things easier. With pretty colours.
sub coloured_date
{
	my ($date, @highlight) = @_;
	my $colour = 'red';
	$colour = 'green' if grep { $_->{month} eq $date->{month} && $_->{day} eq $date->{day} } @highlight;
	return colored("$date->{month} $date->{day}", $colour);
}
sub dates
{
	my (@highlight) = @_;
	return join(', ', map { coloured_date($_, @highlight) } @DATES);
}


sub dates_sharing_this_day
{
	my ($candidate, @dates) = @_;
	return grep { $_->{day} eq $candidate->{day} } @dates;
}

sub dates_sharing_this_month
{
	my ($candidate, @dates) = @_;
	return grep { $_->{month} eq $candidate->{month} } @dates;
}

sub month_has_uniquely_determining_day
{
	my ($candidate, @dates) = @_;
	my @dates_with_unique_days = grep { dates_sharing_this_day($_, @dates) == 1 } @dates;
	return grep { $_->{month} eq $candidate->{month} } @dates_with_unique_days;
}


my @result = @DATES;
say "All dates Cheryl mentions:\n ", dates(@result);

say "\nAlbert doesn't immediately know; there are no unique months:\n ", dates(@result =
	grep { dates_sharing_this_month($_, @DATES) > 1 } @result);

say "Albert believes Bernard cannot possibly know:\n ", dates(@result =
	grep { dates_sharing_this_day($_, @DATES) > 1 } @result);

say "which implies that Albert has a month that wouldn't give Bernard a unique day:\n ", dates(@result =
	grep { ! month_has_uniquely_determining_day($_, @DATES) } @result);

say "\nBernard now says he knows the answer.\nSo given the remaining dates, he must have a day that uniquely determines the month:\n ", dates(@result =
	grep { dates_sharing_this_day($_, @result) == 1 } @result);

say "\nThis prompts Albert to say that he now also knows.\nSo given the remaining dates, he must have a month that uniquely determines the day:\n ", dates(@result =
	grep { dates_sharing_this_month($_, @result) == 1 } @result);
