#### Note: HTML has been manually tinkered with to add output of `./cheryl.pl | aha`

So for some reason, the entire internet *(possibly an exaggeration)* has latched onto this Cheryl's Birthday Problem. It's a logic puzzle where you can deduce the solution while knowing only a few facts about what other people in the puzzle know. I first stumbled across this [Prolog](http://jxf.me/entries/cheryls-birthday/) solution which explains the problem quite nicely - and then found people had already started implementing their own solutions in e.g. [Java](https://github.com/mad4j/puzzles/blob/master/src/dolmisani/puzzles/CherylBirthday.java), which made me decide to spend a lazy Monday afternoon hacking away on my own Perl 5 version.

I thought she calls herself Crystal now?
----------------------------------------

The problem is worded like this:-

*Albert and Bernard have just met Cheryl. “When is your birthday?” Albert asked Cheryl. Cheryl thought for a moment and said, “I won’t tell you, but I’ll give you some clues”. She wrote down a list of ten dates:*

 * May 15, May 16, May 19
 * June 17, June 18
 * July 14, July 16
 * August 14, August 15, August 17

*“One of these is my birthday,” she said.*

*Cheryl whispered in Albert’s ear the month, and only the month, of her birthday. To Bernard, she whispered the day, and only the day. “Can you figure it out now?” she asked Albert.*

 * Albert: “I don’t know when your birthday is, but I know Bernard doesn’t know, either.”
 * Bernard: “I didn’t know originally, but now I do.”
 * Albert: “Well, now I know, too!”

*When is Cheryl’s birthday?*

Working through this by hand, I initially got the wrong answer. The thing to remember about these puzzles is they're not **just** logic puzzles - they also exercise one's English skills, because you have to parse each statement of the puzzle and determine **exactly** what each is implying. In my humble opinion, that first statement by Albert could be better written as "I don't know when your birthday is, but I know Bernard *cannot possibly* know, either.". Because this tells us that part of Albert's knowledge implies that Bernard has an unsolvable problem - at least, in the first iteration of events.

Or was it Carol?
----------------

Let's get to the code. To set things up, we need to put in the selection of possible dates Cheryl first mentions. The easiest way to do this is to first make a list using Perl's "quote (words)" operator, `qw//`, and then split those up into little hashes of month and day.

#highlight 38-44 cheryl.pl

Unlike other solutions, I want to be a bit fancier about the output than just printing the solution. I want to see how each step of the process removes possible answers from the problem until only the solution is left. To do that, let's use a splash of [Term::ANSIColor](http://metacpan.org/pod/Term::ANSIColor):-

#highlight 47-58 cheryl.pl

We also need to declare a few 'predicates' to help us select answers more naturally. I prefer to choose excessively verbose names for these rather than things like 'Day()' and 'Month()' because readability is important when working through these complicated logic problems, not "amount of ink used". Also worth noting: These three subs all return the value of a call to Perl's [grep](http://perldoc.perl.org/functions/grep.html). So they'll return a list of matching dates, if any - and when we need to test "does this predicate come up with a unique solution?", since it's Perl, all we need to do is test if it's `== 1`, because that will force scalar context and test the *number of* results instead of the results themselves. It's nice like that.

#highlight 61-78 cheryl.pl

Okay, on to the actual working-through of the problem!

#highlight 81-97 cheryl.pl

I'm not sure I can easily put the coloured output of the program here without taking a screenshot of text, which is kinda dumb... but hey, why don't you just run it yourself to find the answer? It only requires perl 5.10 for the 'say', really. You can see the full source on my bitbucket here: [cheryl.pl](https://bitbucket.org/jamesneko/blogscripts/src/e8a30f712b165c541b3e7fd51170e3a005de6d0a/cheryl/cheryl.pl)
