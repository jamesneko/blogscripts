class Die {
	has $.value;
	
	method roll {
		$!value = (1..6).pick;
	}
}

my @dice = [ Die.new, Die.new, Die.new ];
@dice».roll;
say "Rolled: ", @dice».value;
