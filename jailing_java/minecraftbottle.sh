#!/bin/sh

# Go to the directory where this script is located. Everything is relative from there.
cd "$(dirname "$0")"
export HOME="$(pwd)/home"
echo "Rehoming to $HOME"
mkdir -p "$HOME"

# Certain programs have in the past given me a small amount of trouble with Minecraft.
#echo "DIE FLASH DIE DIE DIE DIE"
#killall npviewer.bin
#echo "DIE PULSEAUDIO DIE DIE"
#killall pulseaudio

# Minecraft doesn't get along with ibus for some reason.
# I've since switched to fcitx but I'll play it safe.
export XMODIFIERS=''

cd client/
java -Duser.home="$HOME" -jar Minecraft.jar
