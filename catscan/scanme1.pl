#!/usr/bin/perl

use warnings;
use strict;

my $DIR = "$ENV{HOME}/Desktop/";
my $FORMAT = "scan-%06.6d.png";
my $SCANIMAGE = "/usr/bin/scanimage";
my $CONVERT = "/usr/bin/convert";

-x $SCANIMAGE or die "You don't have scanimage installed?!";
-x $CONVERT or die "You don't have ImageMagick installed?!";
chdir($DIR) or die "Couldn't chdir into output dir, $!";

my $outfile;
for (my $i = 0; $i < 1000000; $i++) {
	$outfile = sprintf($FORMAT, $i);
	last if ( ! -f $outfile);
}

print STDERR "Output file: $outfile\n";
system("$SCANIMAGE | $CONVERT - $outfile");
