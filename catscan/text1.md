Another small life update: I've moved. Things are still in boxes, but it's nice to finally be able to de-stress from this situation. One thing about signing a lease and sorting out rental bonds and all that stuff: it generates **a lot of paperwork**. The sort of paperwork I'd like to scan so I have a proper digital copy in my records. While there are plenty of frontends to *SANE*, the linux scanner library, I find a lot of the GUIs a bit slow to use. You put something in, preview it, then scan it, then click save, then a save dialog pops up and you have to name it ... for each page you want to do.

What I really want is a little script to one-shot scan stuff and pick a sensible name for me. So let's write one!

Getting Sane
------------

This isn't the sort of huge application that we build from scratch in C++. No, this is something that a little shell script could solve. Except bash scripting kinda sucks once you get past a certain level of complexity, and as you all know I love Perl, so let's just script it up in Perl 5.

There's also a command-line utility that does most of the work for us already. `scanimage`, part of the `sane-utils` package, can do 90% of what I want. Without any options, it picks the first scanner that it can find, scans it with default settings, and spits it out to `stdout` as a PNM-style image. Looking at the manpage, it even supports a batch mode, but not quite in the manner I was hoping for. I want it to pick a filename, and figure out the numbering by itself if some files in the sequence exist already, so that you can stop and resume later.

To check you've got the scanner available and working, run:-

    scanimage -L

to list available scanners. We'll then run the output of `scanimage` through the `convert` utility, part of the `imagemagick` package. That way, we can save in a file format like PNG which is a bit more widespread than the PNM family and will have a smaller filesize.

Let's write some Perl in a very shellscripty style. I actually converted this from an initial Bash attempt after I got a bit frustrated with Bash's variable manipulation ability. First, we declare a few variables for output directories, a file name template, and where to find the utility programs:-

#highlight 1-9 scanme1.pl

And it's always a good idea to check these things are installed and everything looks good before attempting to start the program proper:-

#highlight 11-13 scanme1.pl

The name-handling logic of the script works like this: Pick a name, format it with `sprintf()` so that it has a unique index. If that file exists already, increment the index and try that name - keep going until you find a free slot. This will make the script generate names like `scan-000005.png`. You can keep running it for every page and rename them later. If you forgot to clean out some past scans, it won't clobber them.

#highlight 15-22 scanme1.pl

Once it's figured out a suitable filename, the actual scanning is easy to do as a one-line shell command, invoked using perl's `system()` call. This version takes one argument and passes it to bash to interpret.

Staying Sane
------------

