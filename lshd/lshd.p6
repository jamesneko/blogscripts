#!/usr/bin/env perl6

use v6;

# Since the purpose of this script is to *identify* drives, it will report drive capacities using
# the decimal notation that the manufacturers like to use rather than MiB, GiB etc.
constant KB = 1000;
constant MB = 1000 ** 2;
constant GB = 1000 ** 3;
constant TB = 1000 ** 4;
constant SYS_BLOCK_SIZE = 512;

# Make the assumption that we're only interested in sda, sdb, etc. Other drive controller types get other naming schemes.
my @drive_paths = dir("/sys/block/", test => /^sd/);

for @drive_paths -> $drive_path {
	my $drive = $drive_path.basename;	# dir returns IO::Path objects, which we can call .basename on!
	say "$drive: " ~ model($drive_path) ~ " " ~ capacity($drive_path);
}


sub model($drive_path)
{
	my $model_path = $drive_path ~ "/device/model";
	try {
		return $model_path.IO.slurp.chomp;
	}
	return "Unknown Model";
}


sub capacity($drive_path)
{
	my $size_path = $drive_path ~ "/size";
	my Int $size;
	try {
		$size = $size_path.IO.slurp.chomp.Int;
		CATCH {
			default { return "Unknown Capacity" };
		}
	}
	# The /sys fs returns capacity in terms of 512-byte blocks, for Reasons.
	$size *= SYS_BLOCK_SIZE;

	# Perl 6's "switch" is given/when. In this case, note we are using the "Whatever Star" in place of
	# $size so we can use an infix operator in the "when" clause. If we just wanted a switch comparing
	# against fixed values, that'd be 'when 42 {}', and $size would be smartmatched against that.
	given $size {
		when * div TB { return round($size / TB, 0.1) ~ " TB" }
		when * div GB { return round($size / GB, 0.1) ~ " GB" }
		when * div MB { return round($size / MB, 0.1) ~ " MB" }
		default { return round($size / KB, 0.1) ~ " KB" }
	}
}
