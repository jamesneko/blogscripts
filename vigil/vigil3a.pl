sub load_md5sum_file
{
	my ($filename) = @_;
	my @plan;
	
	# Yep, we're assuming everything is utf8. Things which are not utf8 cause you pain. Horrible icky pain.
	open(my $fh, '<:utf8', $filename) or die "Couldn't open '$filename' : $!\n";
	my $linenum = 0;
	while (my $line = <$fh>) {
		chomp $line;
		$linenum++;
		if ($line =~ /^(?<md5>\p{ASCII_Hex_Digit}{32})  (?<filename>.*)$/) {
			# Checksum and filename compatible with md5sum output.
			push @plan, create_plan_for_filename($+{filename}, $+{md5});
			
		} elsif ($line =~ /^(?<md5>\p{ASCII_Hex_Digit}{32})  (?<filename>.*)$/) {
			# Checksum and filename compatible with md5sum's manpage but not valid for the actual program.
			# We'll use it, but complain.
			print STDERR colored("Warning: ", 'bold red'), colored("md5sum entry '", 'red'), $line, colored("' on line $linenum of file $filename is using only one space, not two - this doesn't match the output of the actual md5sum program!.", 'red'), "\n";
			push @plan, create_plan_for_filename($+{filename}, $+{md5});
			
		} elsif ($line =~ /^\s*$/) {
			# Blank line, ignore.
			
		} else {
			# No idea. Best not to keep quiet, it could be a malformed checksum line and we don't want to just quietly skip the file if so.
			print STDERR colored("Warning: ", 'bold red'), colored("Unrecognised md5sum entry '", 'red'), $line, colored("' on line $linenum of file $filename.", 'red'), "\n";
			push @plan, { error => "Unrecognised md5sum entry" };
		}
	}
	close($fh) or die "Couldn't close '$filename' : $!\n";
	
	return @plan;
}


sub save_md5sum_file
{
	my ($filename, @plan) = @_;
	
	open(my $fh, '>:utf8', $filename) or die "Couldn't write to '$filename' : $!\n";
	foreach my $plan_entry (@plan) {
		next unless $plan_entry->{correct_md5};
		next unless $plan_entry->{filename};
		print $fh "$plan_entry->{correct_md5}  $plan_entry->{filename}\n";
	}
	close($fh) or die "Couldn't close '$filename' : $!\n";
}