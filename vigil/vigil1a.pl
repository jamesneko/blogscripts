#!/usr/bin/perl -CSDA

use strict;
use warnings;
use utf8;
use Getopt::Long;
use Digest::MD5;
use Term::ANSIColor;

# Define our command-line arguments.
my %opts = ( 'blocksize' => 16384 );
GetOptions(\%opts, "verify=s", "create=s", "update=s", "files", "blocksize=s", "help!");

# Fix up --blocksize argument if any
$opts{blocksize} *= 1024 if $opts{blocksize} =~ /^\d+[km]$/i;
$opts{blocksize} *= 1024 if $opts{blocksize} =~ /^\d+[m]$/i;
die "--blocksize must a number be greater than 0, optionally with k or m suffixes!" unless $opts{blocksize} > 0;


sub help
{
	print "Usage:-\n";
	print "  $0 --verify filename.md5sum\n";
	print "  $0 --create filename.md5sum\n";
	print "  $0 --update filename.md5sum\n";
	print "  $0 --files file_to_check ...\n";
	print "\n";
	print "     --blocksize <size>{k,m}\n";
	exit 1;
}

...

sub main
{
	if ($opts{files}) {
		# Just print out the sums of all the files given as @ARGV.
		display_file_sums(@ARGV);
	} else {
		help();
	}
}


help() if $opts{help};
main();