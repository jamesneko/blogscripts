sub display_file_sums
{
	my (@filenames) = @_;
	
	# Initialise our plan.
	my @plan = map { create_plan_for_filename($_) } @filenames;
	
	# How big is this going be?
	my $total_size = get_plan_size(@plan);
	my $total_progress_so_far = 0;		# updated after each file is processed.
	
	# Initialise the progress bar.
	my $bar = Term::ProgressBar->new({
				name => "Computing MD5",
				count => $total_size,
				ETA => 'linear',
			});
	$bar->minor(0);		# No asterisks used for progress animation.
	my $next_bar_update = 0;		# used to ensure we don't spend more time drawing the bar than actual work.
	
	# Do the sums.
	foreach my $plan_entry (@plan) {
		$bar->message($plan_entry->{filename});
		run_md5_file($plan_entry,
				sub {
					my $progress = $total_progress_so_far + $_[0];
					if ($progress > $next_bar_update) {
						$next_bar_update = $bar->update($progress);
					}
				});
		
		$total_progress_so_far += $plan_entry->{size};
		$next_bar_update = $bar->update($total_progress_so_far);
	}
	$bar->update($total_progress_so_far);
	print STDERR "\n";
	
	# Show the user.
	print_plan_sums(@plan);
}


sub get_plan_size
{
	my (@plan) = @_;
	my $total_size = 0;
	foreach my $entry (@plan) {
		$total_size += $entry->{size} // 0;
	}
	return $total_size;
}