# plan entries are hashrefs with keys: filename, size (bytes), correct_md5 (optional),
# at verification end, additional keys are: computed_md5, start_time (unix timestamp), elapsed_time (secs).
# This way we can attempt an accurate time estimate.
sub create_plan_for_filename
{
	my ($filename, $correct_md5) = @_;
	
	my $plan_entry = {};
	$plan_entry->{filename} = $filename;
	
	if (-f $filename) {
		$plan_entry->{size} = -s $filename;
	} else {
		# Doesn't exist or is a directory or a socket or something. Die?
	}
	
	# If the md5 is supplied by the caller, then we're checking a file. If not, we're probably creating one.
	if (defined $correct_md5) {
		$plan_entry->{correct_md5} = $correct_md5;
	}
	
	return $plan_entry;
}


# No matter whether we're verifying or creating a checksum, we're basically gonna run the same thing over all the
# files given to us in the plan: stream bytes through md5, report progress as we do so.
# Only once the plan has been executed do we *really* need to report which files don't match, although a running
# alert for failing files would be nice too.
sub run_md5_file
{
	my ($plan_entry, $progress_fn) = @_;
	
	# We use the OO interface to Digest::MD5 so we can feed it data a chunk at a time.
	my $md5 = Digest::MD5->new();
	my $current_bytes_read = 0;
	my $buffer;
	$plan_entry->{start_time} = time();
	$plan_entry->{elapsed_time} = 0;
	$plan_entry->{elapsed_bytes} = 0;
	
	# 3 argument form of open() allows us to specify 'raw' directly instead of using binmode and is a bit more modern.
	open(my $fh, '<:raw', $plan_entry->{filename}) or die "Couldn't open file $plan_entry->{filename}, $!\n";
	
	# Read the file in chunks and feed into md5.
	while ($current_bytes_read = read($fh, $buffer, $opts{blocksize})) {
		$md5->add($buffer);
		$plan_entry->{elapsed_bytes} += $current_bytes_read;
		$plan_entry->{elapsed_time} = time() - $plan_entry->{start_time};
		&$progress_fn($plan_entry->{elapsed_bytes});
	}
	# The loop will exit as soon as read() returns 0 or undef. 0 is normal EOF, undef indicates an error.
	die "Error while reading $plan_entry->{filename}, $!\n" if ( ! defined $current_bytes_read);
	
	close($fh) or die "Couldn't close file $plan_entry->{filename}, $!\n";
	
	# We made it out of the file alive. Store the md5 we computed. Note that this resets the Digest::MD5 object.
	$plan_entry->{computed_md5} = $md5->hexdigest();
}