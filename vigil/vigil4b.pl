sub update_file_sums
{
	my ($md5sum_filename, @dirs_and_files_to_sum) = @_;
	
	# We need to load both the contents of our .md5sum file and build an index of the files in given directories,
	# so that we can compare them.
	my @official_plan = load_md5sum_file($md5sum_filename);
	my @actual_plan = build_plan_from_directories(@dirs_and_files_to_sum);
	
	my %official_filenames = map { $_->{filename}, $_ } grep { defined $_->{filename} } @official_plan;
	my %actual_filenames = map { $_->{filename}, $_ } grep { defined $_->{filename} } @actual_plan;
	
	my @new_files = grep { $_->{filename} && ! defined $official_filenames{$_->{filename}} } @actual_plan;
	my @removed_files = grep { $_->{filename} && ! defined $actual_filenames{$_->{filename}} } @official_plan;

	if (scalar @removed_files == scalar keys %official_filenames) {
		print STDERR colored("All files mentioned in '$md5sum_filename' would be removed - refusing to act, something doesn't look right here.", 'bold red'), "\n";
		print STDERR colored("If you really want to do this, use --create to create a brand-new .md5sum file instead.", 'bold red'), "\n";
		exit 1;
	}
	if (@removed_files == 0 && @new_files == 0) {
		print STDERR colored("No changes necessary.", 'bold green'), "\n";
		exit 0;
	}

	if (@removed_files) {
		print STDERR colored(scalar @removed_files . " Removed files:-", 'red'), "\n";
		print_plan_sums(@removed_files);
	}
	if (@new_files) {	
		print STDERR colored(scalar @new_files . " New files:-", 'cyan'), "\n";
	}
	# Go sum the new files.
	run_md5_files_with_progress("Updating MD5", @new_files);
	
	# Remove the removed files and add the new files. Leave unchanged entries as per the original.
	my @updated_plan;
	foreach my $plan_entry (@official_plan, @new_files) {
		# Skip files that can no longer be found.
		next unless $plan_entry->{filename} && defined $actual_filenames{$plan_entry->{filename}};
		
		# If there isn't a {correct_md5} attribute, we should set it from the {computed_md5}.
		$plan_entry->{correct_md5} //= $plan_entry->{computed_md5};
		
		push @updated_plan, $plan_entry;
	}

	print STDERR colored("Saving file '$md5sum_filename'...", 'bold green');
	save_md5sum_file($md5sum_filename, @updated_plan);
	print STDERR colored(" Done!", 'bold green'), "\n";
}