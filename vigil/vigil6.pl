#!/usr/bin/perl -CSDA

# vigil.pl - an improved md5 checksumming program with progress bar and colour.
# Copyright 2014, 2015 James Clark. All rights reserved.
# This program is Free software; you can redistribute it and/or modify it under the same terms as Perl itself.

# $Id: vigil.pl 685 2015-01-21 09:43:19Z james $

# Mental note:
# 	paranoia:
# 		Jan 21 20:22:38 yang kernel: [  407.443008] BTRFS: checksum error at logical 75243085824 on dev /dev/sda2, sector 146959152, root 258, inode 65109, offset 328622080, length 4096, links 1 (path: .ecryptfs/james/.Private/ECRYPTFS_FNEK_ENCRYPTED.FWbEGECaX2Gl9ETDtCEXHc2AltfewDjL-4PUNOsJD.M3f9Di6NBwiTaBv---/ECRYPTFS_FNEK_ENCRYPTED.FWbEGECaX2Gl9ETDtCEXHc2AltfewDjL-4PUrIWwc7Ey9VRycf6cUrT6bE--/ECRYPTFS_FNEK_ENCRYPTED.FWbEGECaX2Gl9ETDtCEXHc2AltfewDjL-4PUwa9hahsbdfumn3BOlhG.CE--/ECRYPTFS_FNEK_ENCRYPTED.FWbEGECaX2Gl9ETDtCEXHc2AltfewDjL-4PUcEYO5xQZufoKA3FGdF2LN---/ECRYPTFS_FNEK_ENCRYPTED.FWbEGECaX2Gl9ETDtCEXHc2AltfewDjL-4PUjV7anW8VbGcD8-hF4x4GlE--/ECRYPTFS_FNEK_ENCRYPTED.FWbEGECaX2Gl9ETDtCEXHc2AltfewDjL-4PU7JvDgeleG.fCNnuDJEDQY---/ECRYPTFS_FNEK_ENCRYPTED.FWbEGECaX2Gl9ETDtCEXHc2AltfewDjL-4PUfolotv7pNNn7M-36oPbHo---/ECRYPTFS_FNEK_ENCRYPTED.FWbEGECaX2Gl9ETDtCEXHc2AltfewDjL-4PUSIhvnlmE4O85wR8v-WdV.E--/ECRYPTFS_FNEK_ENCRYPTED.FXbEGECaX2Gl9ETDtCEXHc2AltfewDjL-4PU1bG3GRpwLmM0NAn6pTCusXyU2odZhViY3FNfeM-s7AE-)
# 		Jan 21 20:22:38 yang kernel: [  407.443016] BTRFS: bdev /dev/sda2 errs: wr 0, rd 0, flush 0, corrupt 1, gen 0
# 		Jan 21 20:22:38 yang kernel: [  407.443017] BTRFS: unable to fixup (regular) error at logical 75246080000 on dev /dev/sda2


use strict;
use warnings;
use utf8;
use 5.014;
use Encode qw/encode_utf8 decode_utf8/;
use Getopt::Long;
use Digest::MD5;
use Term::ANSIColor;
use Term::ProgressBar;
use File::Find;
use File::Basename;
use File::Spec;
use Cwd qw/cwd abs_path/;

use Data::Dumper;

use JSON;	# libjson-perl

my $VIGIL_FILE_VERSION = 1;

sub help
{
	my ($error) = @_;
	my $cmd = basename($0);
	my $help = <<EOF;
		Usage:-
			$cmd <root dir for new .vigil file> --init
				Initialises a new .vigil file to store checksums and other metadata in.
				It will be placed in the given directory, or the current one if none
				is supplied.
				A number of options can be set on the .vigil file to control what
				files get tracked by default:
			
				$cmd <.vigil dir> --match-files '\\.(jpg|png)\$'
				$cmd <.vigil dir> --match-paths '^(Holiday|Birthday).*/'
					Sets a regex to filter newly added files with, if we are just adding
					in bulk. match-files matches on the file basename, match-paths filters
					based on the full pathname relative to the .vigil file.
				
				$cmd <.vigil dir> --ignore-files '^\\.*\$'
				$cmd <.vigil dir> --ignore-paths 'Pornography/'
					Sets a regex to filter newly added files with, if we are just adding
					in bulk. ignore-files skips files based on basename, ignore-paths
					will filter out based on the full pathname relative to the .vigil file.
					The .vigil file itself is always ignored.
				
				Using init by itself only creates the metadata file; to compute checksums
				of all the files monitored by that .vigil file, you must then run:
			
			$cmd <.vigil dir> --list
				Current status. Doesn't change anything.
				
			$cmd <.vigil dir> --add-new
				For files that are not mentioned in the .vigil file but present on
				the filesystem, add them.
				
			$cmd <.vigil dir> --remove-missing
				The .vigil file may mention files that are no longer present on the
				filesystem. Remove those entries.
				
			$cmd <.vigil dir> --update-existing
				For files that are already known to the .vigil file, compute their
				checksums and then record them as being the correct ones. Note this
				will not verify things first, and you may end up recording a sum
				as 'correct' even though you've experienced corruption! Use with care!
			
			$cmd <.vigil dir> --add-and-remove
				Goes through the directory and all subdirectories managed by the .vigil
				file, adding any new files permitted by the match/ignore rules, and
				removing the entries for files that no longer exist.
			
			$cmd <.vigil dir> --verify
				Verifies the checksums of all the managed files.
				
		For md5sum compatibility:-
			$cmd --verify-md5 filename.md5sum
			$cmd --create-md5 filename.md5sum <files and dirs to check>
			$cmd --update-md5 filename.md5sum <files and dirs to check>
			$cmd --just-sum-md5 <files and dirs to check>
		
		Other options controling program behaviour:-
			--blocksize <size>{k,m}
			--vigilfile '.vigil'
EOF
	$help =~ s/^\t\t//gm;
	$help =~ s/\t/   /g;
	$help =~ s/($cmd)/ colored($1, 'bold') /gme;
	$help =~ s/(--\S*)/ colored($1, 'bold') /gme;
	$help =~ s/(<.*?>)/ colored($1, 'cyan') /gme;
	say STDERR colored($error, 'bold red') if $error;
	say STDERR $help;
	exit 1;
}

# Define our command-line arguments.
my %opts = ( blocksize => 16384, vigilfile => '.vigil' );
GetOptions(\%opts, 
		"init",
		"match-files=s", "match-paths=s", "ignore-files=s", "ignore-paths=s",
		"list",
		"add-new", "remove-missing", "update-existing",
		"add-and-remove",
		"verify",
		"verify-md5=s", "create-md5=s", "update-md5=s", "just-sum-md5",
		"blocksize=s", "vigilfile=s", "clobber!",
		"help!"
	);

# Fix up --blocksize argument if any
$opts{blocksize} *= 1024 if $opts{blocksize} =~ /^\d+[km]$/i;
$opts{blocksize} *= 1024 if $opts{blocksize} =~ /^\d+[m]$/i;
help("--blocksize must a number be greater than 0, optionally with k or m suffixes!") unless $opts{blocksize} > 0;



sub main
{
	# --add-and-remove is an alias for --add-new --remove-missing
	if ($opts{'add-and-remove'}) {
		$opts{'add-new'} = 1;
		$opts{'remove-missing'} = 1;
	}
	
	# --help will show help and terminate immediately.
	# Alternatively, if we don't have any primary command arguments specified, also show help.
	help() if $opts{help};
	help() unless $opts{'just-sum-md5'} || $opts{'create-md5'} || $opts{'update-md5'} || $opts{'verify-md5'}
			|| $opts{'init'} || $opts{'list'}
			|| $opts{'add-new'} || $opts{'remove-missing'} || $opts{'update-existing'}
			|| $opts{'verify'};
	
	# legacy md5sum options just do their job and get out.
	if ($opts{'just-sum-md5'}) {
		# Just print out the sums of all the files given as @ARGV.
		display_file_sums(@ARGV);
		exit 0;
		
	} elsif ($opts{'create-md5'}) {
		# Compute the sums of all the files given as @ARGV or found recursively within directories given;
		# Create a new .md5sum file with the name specified after --create-md5.
		create_file_sums($opts{'create-md5'}, @ARGV);
		exit 0;

	} elsif ($opts{'update-md5'}) {
		# Compute the sums of all the files given as @ARGV or found recursively within directories given;
		# But only if they're files not already mentioned in the .md5sum file.
		# Also take note of any files that are mentioned in the file but are now absent.
		# Create a new .md5sum file with updated checksums for the new files and remove absent file sums.
		update_file_sums($opts{'update-md5'}, @ARGV);
		exit 0;

	} elsif ($opts{'verify-md5'}) {
		# Compute the sums of all the files listed in the .md5sum file we get from --verify-md5.
		# Tell us if there are mismatches.
		verify_file_sums($opts{'verify-md5'});
		exit 0;
	}

	# OK! We are in next-gen vigil.pl mode.
	
	# --init new vigil file, or load an existing one.
	my $vigil;
	if ($opts{'init'}) {
		# Drop a new .vigil file.
		my $searchpath = abs_path($ARGV[0] // cwd());
		my $vigilfile = $searchpath . '/' . $opts{vigilfile};
		die "File '$vigilfile' already exists! Refusing to clobber it. Remove the file yourself or Use --clobber if you're certain.\n" if -f $vigilfile && ! $opts{clobber};
		
		$vigil = new_vigil_object($searchpath);
		set_vigil_match_options($vigil);
		$vigil->{dir} = $searchpath;
		$vigil->{filename} = $vigilfile;
		
		save_vigil_file($vigilfile, $vigil);
	} else {
		# We're not making a new one, so we should be loading one.
		my $searchpath = abs_path($ARGV[0] // cwd());
		my $vigilfile = find_vigil_file($searchpath);
		help("No '$opts{vigilfile}' file found in this directory ($searchpath) or ones above it.") unless $vigilfile;
		
		$vigil = load_vigil_file($vigilfile);
		# We always update the 'dir' and 'filename'attribute on the $vigil, just for internal use while we are running.
		# Don't trust its value from a newly loaded file.
		$vigil->{dir} = dirname($vigilfile);
		$vigil->{filename} = $vigilfile;
	}

	# We have a $vigil file now.
	print_vigil_metadata($vigil);
	chdir($vigil->{dir});	# We chdir to be absolutely sure relative paths work how we expect.

	# What can we do with it?
	if ($opts{'list'}) {
		my @known_files = @{$vigil->{files}};
		my @current_files = find_files_from_vigil_dir($vigil);
		my @files = merge_duplicate_files(@known_files, @current_files);
		tag_files($vigil, @files);

		print_files_status($vigil->{dir}, @files);
	}
	
	if ($opts{'add-new'} || $opts{'remove-missing'} || $opts{'update-existing'}) {
		my @known_files = @{$vigil->{files}};
		my @current_files = find_files_from_vigil_dir($vigil);
		my @files = merge_duplicate_files(@known_files, @current_files);
		tag_files($vigil, @files);
		my $changes = 0;

		# We ignore ignored files. Obviously.
		@files = grep { ! $_->{tags}->{ignore} } @files;
		my @missing_files = grep { $_->{tags}->{missing} } @files;
		my @new_files = grep { $_->{tags}->{new} } @files;

		if ($opts{'remove-missing'}) {
			if (@missing_files + @new_files >= @files) {
				say STDERR colored("Warning: all files are either new or would be removed. This seems suspect; refusing to act.", 'bold red');
				exit 1;
			} elsif (@missing_files > 0) {
				@files = @missing_files;
				say STDERR colored(scalar @missing_files . " missing files removed.", 'bold yellow');
				$changes++;
			} else {
				say STDERR colored("No files to remove.", 'bold yellow');
			}
		}
		
		if ($opts{'add-new'}) {
			if (@new_files) {
				run_md5_files_with_progress("Adding New Files", @new_files);
				foreach my $file (@new_files) {
					# If there isn't a {correct_md5} attribute... and there shouldn't be... we should set it from the {computed_md5}.
					$file->{correct_md5} //= $file->{run}->{computed_md5};
					$file->{last_updated} = scalar time();
					$file->{correct_on} = scalar time() if $file->{correct_md5} eq $file->{run}->{computed_md5};
				}
				say STDERR colored(scalar @new_files . " new files added.", 'bold yellow');
				$changes++;
			} else {
				say STDERR colored("No files to add.", 'bold yellow');
			}
		}
		
		if ($opts{'update-existing'}) {
			# After potentially removing and adding new files, what ones remain that we haven't computed an md5 for _this run_?
			my @files_to_update = grep { ! defined $_->{run}->{computed_md5} && ! $_->{tags}->{missing} } @files;
			if (@files_to_update) {
				run_md5_files_with_progress("Updating Existing Files", @files_to_update);
				foreach my $file (@files_to_update) {
					# Forcibly change the correct md5 to the one we just computed.
					$file->{correct_md5} = $file->{run}->{computed_md5};
					$file->{last_updated} = scalar time();
					$file->{correct_on} = scalar time();
					#### FIXME: Also do size. But also; what if we get a file error here? What if it's gone?
					#### We probably want to keep errors around, in case we are running this in an automated script.
					#### So we probably want to add a --clear-errors in this block somewhere.
				}
				say STDERR colored(scalar @files_to_update . " existing files updated.", 'bold yellow');
				$changes++;
			} else {
				say STDERR colored("No existing files to update.", 'bold yellow');
			}
		}
		
		if ($changes) {
			# Set new files list and save.
			$vigil->{files} = [ @files ];
			print_files_status($vigil->{dir}, @files);
			save_vigil_file($vigil->{filename}, $vigil);
		} else {
			say STDERR colored("No changes.", 'bold yellow');
		}
		print_plan_pass_fail_summary(@files);
		print_plan_errors(@files);
	}

	
	if ($opts{'verify'}) {
		# For --verify, we only care about files known to the .vigil file already, ignore the filesystem.
		my @files = @{$vigil->{files}};
		tag_files($vigil, @files);

		# We ignore ignored files. Obviously.
		@files = grep { ! $_->{tags}->{ignore} } @files;
		
		# We can only verify files that are actually present; however, missing files are still an error that needs to be reported.
		my @files_to_verify = grep { ! $_->{tags}->{missing} } @files;
		my @missing_files = grep { $_->{tags}->{missing} } @files;
		if (@files_to_verify) {
			run_md5_files_with_progress("Verifying", @files_to_verify);
			foreach my $file (@files_to_verify) {
				# If there isn't a {correct_md5} attribute... that's a bit weird. They should have gotten one with --add-new.
				next unless $file->{correct_md5};
				$file->{correct_on} = scalar time() if $file->{correct_md5} eq $file->{run}->{computed_md5};
			}

			# We actually do need to save the .vigil file after verifying, because we want to record the updated {correct_on} time.
			print_files_status($vigil->{dir}, @files);
			save_vigil_file($vigil->{filename}, $vigil);
			print_plan_pass_fail_summary(@files);
			print_plan_errors(@files);
		} else {
			say STDERR colored("No files to verify.", 'bold yellow');
		}
	}
}


# plan entries are hashrefs with keys: filename, size (bytes), correct_md5 (optional),
# at verification end, additional keys are: computed_md5, start_time (unix timestamp), elapsed_time (secs).
# This way we can attempt an accurate time estimate.
sub create_plan_for_filename
{
	my ($filename, $correct_md5) = @_;
	
	my $plan_entry = {};
	$plan_entry->{filename} = $filename;
	
	if (-f $filename && -r $filename) {
		$plan_entry->{size} = -s $filename;
	} else {
		# Doesn't exist or is a directory or a socket or something. Die?
		$plan_entry->{error} = "Is not readable" unless -r $filename;
		$plan_entry->{error} = "Is not a plain file" unless -f $filename;
		$plan_entry->{error} = "Is a directory" if -d $filename;
		$plan_entry->{error} = "Does not exist" unless -e $filename;
	}
	
	# If the md5 is supplied by the caller, then we're checking a file. If not, we're probably creating one.
	if (defined $correct_md5) {
		$plan_entry->{correct_md5} = $correct_md5;
	}
	
	return $plan_entry;
}


# No matter whether we're verifying or creating a checksum, we're basically gonna run the same thing over all the
# files given to us in the plan: stream bytes through md5, report progress as we do so.
# Only once the plan has been executed do we *really* need to report which files don't match, although a running
# alert for failing files would be nice too.
sub run_md5_file
{
	my ($plan_entry, $progress_fn) = @_;
	
	# We use the OO interface to Digest::MD5 so we can feed it data a chunk at a time.
	my $md5 = Digest::MD5->new();
	my $current_bytes_read = 0;
	my $buffer;
	$plan_entry->{start_time} = time();
	$plan_entry->{elapsed_time} = 0;
	$plan_entry->{elapsed_bytes} = 0;
	
	# 3 argument form of open() allows us to specify 'raw' directly instead of using binmode and is a bit more modern.
	my $fh;
	unless (open($fh, '<:raw', $plan_entry->{filename})) {
		say STDERR colored("Couldn't open file '$plan_entry->{filename}', $!", 'bold red');
		$plan_entry->{error} = $!;
		&$progress_fn($plan_entry->{size});
		return;
	}
	
	# Read the file in chunks and feed into md5.
	while ($current_bytes_read = read($fh, $buffer, $opts{blocksize})) {
		$md5->add($buffer);
		$plan_entry->{elapsed_bytes} += $current_bytes_read;
		$plan_entry->{elapsed_time} = time() - $plan_entry->{start_time};
		&$progress_fn($plan_entry->{elapsed_bytes});
	}
	# The loop will exit as soon as read() returns 0 or undef. 0 is normal EOF, undef indicates an error.
	if ( ! defined $current_bytes_read) {
		say STDERR colored("Error while reading '$plan_entry->{filename}', $!", 'bold red');
		$plan_entry->{error} = $!;
		&$progress_fn($plan_entry->{size});
		return;
	}
	
	unless (close($fh)) {
		say STDERR colored("Couldn't close file '$plan_entry->{filename}', $!", 'bold red');
		$plan_entry->{error} = $!;
		&$progress_fn($plan_entry->{size});
		return;
	}
	
	# We made it out of the file alive. Store the md5 we computed. Note that this resets the Digest::MD5 object.
	$plan_entry->{run}->{computed_md5} = $md5->hexdigest();
	$plan_entry->{computed_md5} = $plan_entry->{run}->{computed_md5};
	$plan_entry->{computed_on} = scalar time();
}


# While run_md5_file is responsible for processing an individual file, run_md5_files_with_progress is the one
# that is responsible for doing a large batch of files, as well as giving visual feedback via a progress bar.
sub run_md5_files_with_progress
{
	my ($progressbar_title, @plan) = @_;

	# How big is this going be?
	my $total_size = get_plan_size(@plan);
	my $total_progress_so_far = 0;		# updated after each file is processed.
	
	# Initialise the progress bar.
	my $bar = Term::ProgressBar->new({
				name => $progressbar_title,
				count => $total_size,
				ETA => 'linear',
			});
	$bar->minor(0);		# No asterisks used for progress animation.
	my $next_bar_update = 0;		# used to ensure we don't spend more time drawing the bar than actual work.
	
	# Do the sums.
	foreach my $plan_entry (@plan) {
		next unless defined $plan_entry->{filename};
		next unless defined $plan_entry->{size};
		$bar->message($plan_entry->{filename});
		run_md5_file($plan_entry,
				sub {
					my $progress = $total_progress_so_far + $_[0];
					if ($progress > $next_bar_update) {
						$next_bar_update = $bar->update($progress);
					}
				});
		
		$total_progress_so_far += $plan_entry->{size};
		$next_bar_update = $bar->update($total_progress_so_far);
	}
	$bar->update($total_progress_so_far);
	print STDERR "\n";
}


sub verify_file_sums
{
	my ($md5sum_filename) = @_;
	
	# Initialise our plan from the md5sum file.
	my @plan = load_md5sum_file($md5sum_filename);
	
	# Go sum all those files.
	run_md5_files_with_progress("Verifying MD5", @plan);

	# Show the user.
	print_plan_sums(@plan);
	print_plan_pass_fail_summary(@plan);
	print_plan_errors(@plan);
}


sub create_file_sums
{
	my ($md5sum_filename, @dirs_and_files_to_sum) = @_;
	
	die "The file '$md5sum_filename' exists already and does not appear to be writable!\n" if (-e $md5sum_filename && ! -w $md5sum_filename);
	die "The dir '$md5sum_filename' is to be made in does not appear to be writable!\n" if ( ! -e $md5sum_filename && ! -w dirname($md5sum_filename));
	
	# Initialise our plan from the files and directories given on the commandline.
	# These will be relative to whatever the user's perspective is! There is no standard!
	my @plan = find_files_from_directories(@dirs_and_files_to_sum);
	
	# Go sum all those files.
	run_md5_files_with_progress("Creating MD5", @plan);
	
	# Show the user.
	print_plan_sums(@plan);
	print_plan_pass_fail_summary(@plan);
	print_plan_errors(@plan);

	# The checksums we are making are all new, and we can only assume they are all correct.
	foreach my $plan_entry (@plan) {
		# If there isn't a {correct_md5} attribute, we should set it from the {computed_md5}.
		$plan_entry->{correct_md5} //= $plan_entry->{run}->{computed_md5};
	}
	
	# Then write the computed MD5s to the md5sum file given with --create.
	save_md5sum_file($md5sum_filename, @plan);
}


sub update_file_sums
{
	my ($md5sum_filename, @dirs_and_files_to_sum) = @_;
	
	die "The file '$md5sum_filename' does not exist!\n" if ( ! -e $md5sum_filename);
	die "The file '$md5sum_filename' does not appear to be writable!\n" if (-e $md5sum_filename && ! -w $md5sum_filename);

	# We need to load both the contents of our .md5sum file and build an index of the files in given directories,
	# so that we can compare them.
	my @official_plan = load_md5sum_file($md5sum_filename);
	my @actual_plan = find_files_from_directories(@dirs_and_files_to_sum);	# Relative to wherever the user is running!
	
	my %official_filenames = map { $_->{filename}, $_ } grep { defined $_->{filename} } @official_plan;
	my %actual_filenames = map { $_->{filename}, $_ } grep { defined $_->{filename} } @actual_plan;
	
	my @new_files = grep { $_->{filename} && ! defined $official_filenames{$_->{filename}} } @actual_plan;
	my @removed_files = grep { $_->{filename} && ! defined $actual_filenames{$_->{filename}} } @official_plan;

	if (scalar @removed_files == scalar keys %official_filenames) {
		print STDERR colored("All files mentioned in '$md5sum_filename' would be removed - refusing to act, something doesn't look right here.", 'bold red'), "\n";
		print STDERR colored("If you really want to do this, use --create to create a brand-new .md5sum file instead.", 'bold red'), "\n";
		exit 1;
	}
	if (@removed_files == 0 && @new_files == 0) {
		print STDERR colored("No changes necessary.", 'bold green'), "\n";
		exit 0;
	}

	if (@removed_files) {
		print STDERR colored(scalar @removed_files . " Removed files:-", 'red'), "\n";
		print_plan_sums(@removed_files);
	}
	if (@new_files) {	
		print STDERR colored(scalar @new_files . " New files:-", 'cyan'), "\n";
	}
	# Go sum the new files.
	run_md5_files_with_progress("Updating MD5", @new_files);
	
	# Show the user.
	print_plan_sums(@new_files);
	print_plan_pass_fail_summary(@new_files);
	print_plan_errors(@new_files);

	# Remove the removed files and add the new files. Leave unchanged entries as per the original.
	my @updated_plan;
	foreach my $plan_entry (@official_plan, @new_files) {
		# Skip files that can no longer be found.
		next unless $plan_entry->{filename} && defined $actual_filenames{$plan_entry->{filename}};
		
		# If there isn't a {correct_md5} attribute, we should set it from the {computed_md5}.
		$plan_entry->{correct_md5} //= $plan_entry->{run}->{computed_md5};
		
		push @updated_plan, $plan_entry;
	}

	print STDERR colored("Saving file '$md5sum_filename'...", 'bold green');
	save_md5sum_file($md5sum_filename, @updated_plan);
	print STDERR colored(" Done!", 'bold green'), "\n";
}


# For use with 'md5sum' mode; Given a flat list of files and dirs, specified relative to
# wherever the hell the user is coming from, or maybe absolute, who knows, build our
# list of plan entries.
sub find_files_from_directories
{
	my (@dirs_and_files_to_sum) = @_;
	# File::Find works with binary strings from the filesystem only.
	# This means our nice utf8 strings we get in @ARGV (via the -CA perl flag)
	# shouldn't be fed to find() - it must be concatenating them behind the
	# scenes and this causes Amusing Failure when attempting to traverse a directory
	# with utf8 code points. We must convert our UTF-X flagged wide character
	# Perl strings to utf-8 encoded binary data strings before feeding them to
	# the module, and utf8::decode() the filenames we get out of it as we traverse
	# the directory tree.
	my @dirs_and_files_to_sum_bin = map { encode_utf8($_); } @dirs_and_files_to_sum;
	
	# Initialise our plan from the files and directories given on the commandline.
	# Traversing a big tree recursively could take a while, so let the user know.
	print STDERR colored("Scanning directories... ", 'yellow');
	my @plan;
	find(	{
				# We pass in an anonymous sub to File::Find for it to run on every file and directory
				# as it traverses the tree. Note that the parameter name, 'wanted', is a misnomer;
				# File::Find will not do anything with the return value of our sub.
				wanted => sub {
					# Apparently File::Find doesn't handle utf8 filenames.
					my $fullpathname = decode_utf8($File::Find::name);
					return unless (-e $fullpathname && ! -d $fullpathname);
					push @plan, create_plan_for_filename($fullpathname);
				},
				follow_skip => 2,		# If we get duplicate dirs, files etc. during find, ignore and carry on.
				no_chdir => 1,			# We want our current dir consistent after Find does its work.
			},
			@dirs_and_files_to_sum_bin
		);
	@plan = prune_plan_duplicates(@plan);
	print STDERR colored("complete. Plan contains " . scalar @plan . " items.", 'yellow'), "\n";
	
	return @plan;
}


# For use with the new '.vigil' mode. Only finds from the one dir that we live in, no regard to
# what the user's CWD is, the standard is to specify and return all files as paths relative to
# our .vigil file's location.
sub find_files_from_vigil_dir
{
	my ($vigil) = @_;
	my $absbasedir = File::Spec->rel2abs($vigil->{dir});
	# File::Find works with binary strings from the filesystem only.
	# We need to convert our UTF-whatever flagged Perl strings into a utf8 encoded binary string
	# before passing them to find(), and decode the filenames we get from it.
	# If you aren't on a utf-8 filesystem, this will probably fail! And you got what was coming to you.
	my $absbasedir_utf8 = $absbasedir;
	encode_utf8($absbasedir_utf8);
	
	# Initialise our plan from the files and directories given on the commandline.
	# Traversing a big tree recursively could take a while, so let the user know.
	print STDERR colored("Scanning directory... ", 'yellow');
	my @plan;
	find(	{
				# We pass in an anonymous sub to File::Find for it to run on every file and directory
				# as it traverses the tree. Note that the parameter name, 'wanted', is a misnomer;
				# File::Find will not do anything with the return value of our sub.
				wanted => sub {
					# Apparently File::Find doesn't handle utf8 filenames.
					my $fullpathname = decode_utf8($File::Find::name);
					my $basename = basename($fullpathname);
					
					return unless (-e $fullpathname && ! -d $fullpathname);
					# We run the path through abs2rel because we want all stored paths to be relative to
					# the .vigil file, and stored in exactly the same way.
					my $relpathname = File::Spec->abs2rel($fullpathname, $absbasedir);
					
					# Never report our own metadata file.
					return if $basename eq $opts{vigilfile};
					
					push @plan, create_plan_for_filename($relpathname);
				},
				follow_skip => 2,		# If we get duplicate dirs, files etc. during find, ignore and carry on.
				no_chdir => 1,			# We want our current dir consistent after Find does its work.
			},
			$absbasedir_utf8
		);
	@plan = prune_plan_duplicates(@plan);
	print STDERR colored("complete. Directory contains " . scalar @plan . " items.", 'yellow'), "\n";
	
	return @plan;
}


# Set a ->{tags}->{ignore} flag on plan entries that we should be ignoring, making identification in --list possible.
sub tag_files
{
	my ($vigil, @files) = @_;
	foreach my $file (@files) {
		my $relpathname = $file->{filename};
		my $abspathname = File::Spec->rel2abs($relpathname, $vigil->{dir});
		my $basename = basename($relpathname);
		# First clear the tags.
		$file->{tags} = {};
		
		# Figure out if we should ignore this file.
		$file->{tags}->{ignore} = 'ignore_files' if defined $vigil->{ignore_files} &&    $basename =~ /$vigil->{ignore_files}/;
		$file->{tags}->{ignore} = 'ignore_paths' if defined $vigil->{ignore_paths} && $relpathname =~ /$vigil->{ignore_paths}/;
		$file->{tags}->{ignore} = 'match_files'  if defined $vigil->{match_files}  &&    $basename !~ /$vigil->{match_files}/;
		$file->{tags}->{ignore} = 'match_paths'  if defined $vigil->{match_paths}  && $relpathname !~ /$vigil->{match_paths}/;
		
		# Is this file not mentioned in the $vigil?
		$file->{tags}->{new} = 1 unless grep { $_->{filename} eq $relpathname } @{$vigil->{files}};
		
		# Is this file not present on the filesystem?
		$file->{tags}->{missing} = 1 unless -f $abspathname;
	}
	return @files;
}


sub display_file_sums
{
	my (@filenames) = @_;
	
	# Initialise our plan from the filenames given on the commandline.
	my @plan = map { create_plan_for_filename($_) } @filenames;
	
	# Go sum all those files.
	run_md5_files_with_progress("Computing MD5", @plan);
	
	# Show the user.
	print_plan_sums(@plan);
	print_plan_pass_fail_summary(@plan);
	print_plan_errors(@plan);
}


sub prune_plan_duplicates
{
	my (@plan) = @_;
	my @uniq_plan;
	my %names;
	foreach my $plan_entry (@plan) {
		next unless $plan_entry->{filename};
		if ($names{$plan_entry->{filename}} // 0 == 1) {
			print STDERR colored("Warning: File '$plan_entry->{filename}' mentioned multiple times. Duplicate entries in plan will be ignored.", 'bold yellow'), "\n";
		}
		push @uniq_plan, $plan_entry unless $names{$plan_entry->{filename}};
		$names{$plan_entry->{filename}}++;
	}
	return @uniq_plan;
}


# Primarily for md5sum compatibility, no bells or whistles.
sub print_plan_sums
{
	my (@plan) = @_;
	foreach my $plan_entry (@plan) {
		next unless $plan_entry->{filename};
		
		my $sumdata = $plan_entry->{run}->{computed_md5} // $plan_entry->{correct_md5} // "(none)";
		my $sumcolour = "reset";
		if ($plan_entry->{run}->{computed_md5}) {
			$sumcolour = "cyan";
		}
		if ($plan_entry->{run}->{computed_md5} && $plan_entry->{correct_md5}) {
			$sumcolour = ( $plan_entry->{run}->{computed_md5} eq $plan_entry->{correct_md5} ) ? "green" : "red";
		}
		
		# Only output colour if we're on a TTY; user might want to > to a file.
		if (-t STDOUT) {
			print colored($sumdata, $sumcolour) . "  " . $plan_entry->{filename}, "\n";
		} else {
			print "$sumdata  $plan_entry->{filename}\n";
		}
	}
}


# For new '.vigil' mode, show us a summary of what Vigil sees.
sub print_files_status
{
	my ($absbasedir, @files) = @_;
	foreach my $file (@files) {
		next unless $file->{filename};
		my $abspathname = File::Spec->rel2abs($file->{filename}, $absbasedir);
		my $tagtext = "[???]";
		my $tagcolour = 'bold magenta';
		my $filenamecolour = 'reset';
		
		if ($file->{correct_md5}) {
			$tagtext = "[MD5]";
			$tagcolour = 'cyan';
			
			if ($file->{computed_md5}) {
				if ($file->{computed_md5} eq $file->{correct_md5}) {
					$tagtext = "[MD5]";
					$tagcolour = 'bold cyan';
					$filenamecolour = 'green';
				} else {
					$tagtext = "[BAD]";
					$tagcolour = 'red';
					$filenamecolour = 'red';
				}
			}
			if ($file->{run}->{computed_md5}) {
				if ($file->{run}->{computed_md5} eq $file->{correct_md5}) {
					$tagtext = "[OK!]";
					$tagcolour = 'bold green';
					$filenamecolour = 'green';
				} else {
					$tagtext = "[BAD]";
					$tagcolour = 'bold red';
					$filenamecolour = 'red';
				}
			}
		}
		
		if ($file->{tags}->{new}) {
			$tagtext = "[NEW]";
			$tagcolour = 'bold yellow';
			if ($file->{run}->{computed_md5}) {
				if ($file->{run}->{computed_md5} eq $file->{correct_md5}) {
					$tagcolour = 'bold green';
				} else {
					$tagcolour = 'bold red';
				}
			}
		}
				
		if ($file->{tags}->{ignore}) {
			$tagtext = "[IGN]";
			$tagcolour = 'white';
			$filenamecolour = 'white';
		}

		if ($file->{error}) {
			$tagtext = "[ERR]";
			$tagcolour = 'bold red';
			$filenamecolour = 'red';
		}

		if ($file->{tags}->{missing}) {
			$tagtext = "[MIS]";
			$tagcolour = 'yellow';
			$filenamecolour = 'yellow';
		}
	
		# Only output colour if we're on a TTY; user might want to > to a file.
		if (-t STDOUT) {
			say colored($tagtext, $tagcolour) . " " . colored($file->{filename}, $filenamecolour);
		} else {
			say "$tagtext $file->{filename}";
		}
	}
}


# For new '.vigil' mode, merge entries talking about the same ->{filename}.
sub merge_file
{
	my (@identical_files) = @_;
	die "merge_files() called without files to merge!" if @identical_files == 0;
	
	my $file = shift @identical_files;
	while (my $otherfile = shift @identical_files) {
		# Merge keys, giving precedence to the first $file; if that key already exists, keep its version.
		%$file = ( %$otherfile, %$file );
	}
	return $file;
}


# Find file entries that are duplicates and merge them using merge_files() above.
# Note slightly different behaviour to the old prune_plan_duplicates(); we want to smoosh the top-level keys together.
sub merge_duplicate_files
{
	my (@files) = @_;
	my @merged;
	while (my $file = shift @files) {
		# Pull out the duplicates, if any
		my @duplicates = grep { $_->{filename} eq $file->{filename} } @files;
		@files =         grep { $_->{filename} ne $file->{filename} } @files;
		# Merge them, if need be. First $file we come across gets precedence.
		$file = merge_file($file, @duplicates);
		push @merged, $file;
	}
	return @merged;
}


sub print_plan_pass_fail_summary
{
	my (@plan) = @_;
	my @checked = grep { $_->{run}->{computed_md5} } @plan;
	my @verified = grep { $_->{correct_md5} && ( $_->{run}->{computed_md5} // "fail" ) eq $_->{correct_md5} } @plan;
	my @failures = grep { $_->{correct_md5} && ( $_->{run}->{computed_md5} // "fail" ) ne $_->{correct_md5} } @plan;
	my @missing = grep { $_->{tags}->{missing} } @plan;
	my $full_size = get_plan_size(@plan);
	my $elapsed_bytes = get_plan_elapsed_bytes(@plan);
	my $elapsed_time = get_plan_elapsed_time(@plan);
	my $avg_speed = $elapsed_bytes / ($elapsed_time || 1);
	
	if (@plan ) {
		print STDERR colored(scalar @plan . " files considered, " . scalar @checked . " checked.", 'cyan'), "\n";
	}
	if (@plan && $elapsed_bytes > 0) {
		$full_size = format_byte_size($full_size);
		$elapsed_bytes = format_byte_size($elapsed_bytes);
		$elapsed_time = format_time_duration($elapsed_time);
		$avg_speed = format_byte_size($avg_speed);
		print STDERR colored("$elapsed_bytes of $full_size checked in $elapsed_time ($avg_speed/s)", 'cyan'), "\n";
	}
	if (@verified) {
		print STDERR colored(scalar @verified . " files verified OK.", 'green'), "\n";
	}
	if (@missing) {
		print STDERR colored(scalar @missing . " files missing.", 'yellow'), "\n";
	}
	if (@failures) {
		print STDERR colored(scalar @failures . " files failed checksum:-", 'bold red'), "\n";
		foreach my $failure (@failures) {
			print STDERR colored("   " . $failure->{filename}, 'reset'), "\n";
		}
	}
}


sub print_plan_errors
{
	my (@plan) = @_;
	my %files_by_error;
	foreach my $plan_entry (@plan) {
		next unless $plan_entry->{error};
		$files_by_error{$plan_entry->{error}} //= [];
		push @{ $files_by_error{$plan_entry->{error}} }, $plan_entry;
	}
	if (keys %files_by_error > 0) {
		say STDERR colored("Errors were encountered during processing:-", 'bold red');
		foreach my $error (keys %files_by_error) {
			say STDERR colored("   [$error]:-", 'bold red');
			foreach my $failure (@{$files_by_error{$error}}) {
				say STDERR colored("      " . $failure->{filename}, 'reset');
			}
		}
	}
}


sub get_plan_size
{
	return get_plan_summed_field_value('size', @_);
}

sub get_plan_elapsed_bytes
{
	return get_plan_summed_field_value('elapsed_bytes', @_);
}

sub get_plan_elapsed_time
{
	return get_plan_summed_field_value('elapsed_time', @_);
}



sub get_plan_summed_field_value
{
	my ($fieldname, @plan) = @_;
	my $total = 0;
	foreach my $entry (@plan) {
		$total += $entry->{$fieldname} // 0;
	}
	return $total;
}


sub format_byte_size
{
	my ($bytes) = @_;
	my $unit = 'B';
	if ($bytes > 1024 * 1024 * 1024) {
		$bytes /= 1024 * 1024 * 1024;
		$unit = 'GiB';
	} elsif ($bytes > 1024 * 1024) {
		$bytes /= 1024 * 1024;
		$unit = 'MiB';
	} elsif ($bytes > 1024) {
		$bytes /= 1024;
		$unit = 'KiB';
	}
	return sprintf("%.01f %s", $bytes, $unit);
}


sub format_time_duration
{
	my ($total_secs) = @_;
	my $days = "";
	my $hours = "";
	my $mins = "";
	my $secs = "";
	if ($total_secs > 60 * 60 * 24) {
		$days = int($total_secs / (60 * 60 * 24)) . "d";
		$total_secs %= 60 * 60 * 24;
	}
	if ($total_secs > 60 * 60) {
		$hours = int($total_secs / (60 * 60)) . "h";
		$total_secs %= 60 * 60;
	}
	if ($total_secs > 60) {
		$mins = int($total_secs / 60) . "m";
		$total_secs %= 60;
	}
	$secs = $total_secs . "s";
	return "$days$hours$mins$secs";
}


sub load_md5sum_file
{
	my ($filename) = @_;
	my @plan;
	
	# Yep, we're assuming everything is utf8. Things which are not utf8 cause you pain. Horrible icky pain.
	open(my $fh, '<:utf8', $filename) or die "Couldn't open '$filename' : $!\n";
	my $linenum = 0;
	while (my $line = <$fh>) {
		chomp $line;
		$linenum++;
		if ($line =~ /^(?<md5>\p{ASCII_Hex_Digit}{32})  (?<filename>.*)$/) {
			# Checksum and filename compatible with md5sum output.
			push @plan, create_plan_for_filename($+{filename}, $+{md5});
			
		} elsif ($line =~ /^(?<md5>\p{ASCII_Hex_Digit}{32})  (?<filename>.*)$/) {
			# Checksum and filename compatible with md5sum's manpage but not valid for the actual program.
			# We'll use it, but complain.
			print STDERR colored("Warning: ", 'bold red'), colored("md5sum entry '", 'red'), $line, colored("' on line $linenum of file $filename is using only one space, not two - this doesn't match the output of the actual md5sum program!.", 'red'), "\n";
			push @plan, create_plan_for_filename($+{filename}, $+{md5});
			
		} elsif ($line =~ /^\s*$/) {
			# Blank line, ignore.
			
		} else {
			# No idea. Best not to keep quiet, it could be a malformed checksum line and we don't want to just quietly skip the file if so.
			print STDERR colored("Warning: ", 'bold red'), colored("Unrecognised md5sum entry '", 'red'), $line, colored("' on line $linenum of file $filename.", 'red'), "\n";
			push @plan, { error => "Unrecognised md5sum entry" };
		}
	}
	close($fh) or die "Couldn't close '$filename' : $!\n";
	
	return @plan;
}


sub save_md5sum_file
{
	my ($filename, @plan) = @_;
	
	my $fh;
	unless (open($fh, '>:utf8', $filename)) {
		print STDERR colored("Warning: Couldn't write to '$filename' : $!", 'bold red'), "\n";
		$filename = "/tmp/vigil.$$." . basename($filename);
		print STDERR colored("         Attempting to write to '$filename' instead.", 'red'), "\n";
		open($fh, '>:utf8', $filename) or die "Argh, that failed as well! : $!\n";
	}
	foreach my $plan_entry (@plan) {
		next unless $plan_entry->{correct_md5};
		next unless $plan_entry->{filename};
		print $fh "$plan_entry->{correct_md5}  $plan_entry->{filename}\n";
	}
	close($fh) or die "Couldn't close '$filename' : $!\n";
}


# Our initial JSON file structure.
sub new_vigil_object
{
	my ($dirpath) = @_;
	return {
			# match_files
			# match_paths
			# ignore_files
			# ignore_paths
			version => $VIGIL_FILE_VERSION,		# what version of the file structure.
			nickname => basename($dirpath) =~ s{/}{_}gr,
			last_updated => scalar time(),	# When was the .vigil file last saved?
			files => [],
		};
}


sub set_vigil_match_options
{
	my ($vigil) = @_;
	foreach my $opt (qw/match-files match-paths ignore-files ignore-paths/) {
		if (defined $opts{$opt}) {
			say STDERR colored("Setting '$opt' option to '$opts{$opt}'.", 'cyan');
			my $key = $opt =~ s/-/_/gr;
			$vigil->{$key} = $opts{$opt};
		}
	}
}


# Iterate upwards through directories until we find a file called .vigil
sub find_vigil_file
{
	my ($dirname) = @_;
	# This step will probably not work on legacy OSes with volume identifiers in the paths.
	# I'm not coding around all that C: D: cruft. Use cygwin or msys or whatever to fix it maybe.
	# In fact, I don't even wanna consider a path separator that isn't '/'.
	if (-f $dirname . "/" . $opts{vigilfile}) {
		return $dirname . "/" . $opts{vigilfile};
	} else {
		my $up = dirname($dirname);	# returns all but last path entry even if last entry is clearly a dir with a trailing /
		return if $up eq $dirname;		# return undef if we can't find anything.
		return find_vigil_file($up);
	}
}


sub print_vigil_metadata
{
	my ($vigil) = @_;
	say STDERR colored("File: $vigil->{filename}", 'cyan') if $vigil->{filename};
	say STDERR colored("Last modified: " . localtime($vigil->{last_modified}), 'cyan') if $vigil->{last_modified};
	foreach my $opt (qw/match_files match_paths ignore_files ignore_paths/) {
		if (defined $vigil->{$opt}) {
			say STDERR colored("Option: $opt = '$vigil->{$opt}'", 'cyan');
		}
	}
}


sub save_vigil_file
{
	my ($filename, $vigil) = @_;
	# Try and open the file.
	my $fh;
	unless (open($fh, '>:utf8', $filename)) {
		print STDERR colored("Warning: Couldn't write to '$filename' : $!", 'bold red'), "\n";
		$filename = "/tmp/vigil.$$." . $vigil->{nickname} // "blah";
		print STDERR colored("         Attempting to write to '$filename' instead.", 'red'), "\n";
		open($fh, '>:utf8', $filename) or die "Argh, that failed as well! : $!\n";
	}
	# Make a copy that strips out tags and things we don't need.
	$vigil = rerender_vigil_for_save($vigil);
	# Turn what we have into JSON.
	my $json_text = JSON->new->pretty->encode($vigil);
	# And write it.
	print $fh $json_text;
	close($fh) or die "Couldn't close '$filename' : $!\n";
}


sub rerender_vigil_for_save
{
	my ($origvigil) = @_;
	# Map the basic keys over
	my $vigil = { %$origvigil };
	# But remove things we only needed at runtime.
	delete $vigil->{filename};
	delete $vigil->{dir};
	
	# Map all the file entries over.
	$vigil->{files} = [ map { rerender_vigil_file_for_save($_) } @{$origvigil->{files}} ];
	
	# Update the file metadata.
	$vigil->{version} = $VIGIL_FILE_VERSION;
	$vigil->{last_updated} = scalar time();
	return $vigil;
}


sub rerender_vigil_file_for_save
{
	my ($origfile) = @_;
	# Map the basic keys over
	my $file = { %$origfile };
	# But remove things we only needed at runtime.
	delete $file->{tags};
	delete $file->{run};
	delete $file->{elapsed_time};
	delete $file->{elapsed_bytes};
	delete $file->{start_time};
	
	return $file;
}


sub load_vigil_file
{
	my ($filename) = @_;
	# Yep, we're assuming everything is utf8. Things which are not utf8 cause you pain. Horrible icky pain.
	open(my $fh, '<:utf8', $filename) or die "Couldn't open '$filename' : $!\n";
	local $/ = undef;	# unset record separator to slurp entire file in one readline call.
	my $contents = readline $fh;
	close($fh) or die "Couldn't close '$filename' : $!\n";

	# Then interpret it as JSON, and return the hash.
	my $vigil = JSON->new->decode($contents); # croaks on error, that's fine
	
	# Still, we could do some sanity-checks.
	die "JSON in file '$filename' is oddly malformed, expected a hash.\n" unless ref $vigil eq 'HASH';
	die "JSON in file '$filename' is oddly malformed, there's no version number!\n" unless $vigil->{version};
	die "Error: The file '$filename' was created with a newer version of vigil.pl than this one: $vigil->{version} vs our $VIGIL_FILE_VERSION.\n" if $vigil->{version} > $VIGIL_FILE_VERSION;
	return $vigil;
}


main();

