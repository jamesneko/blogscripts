sub display_file_sums
{
	my (@filenames) = @_;
	
	# Initialise our plan.
	my @plan = map { create_plan_for_filename($_) } @filenames;
	
	# Do the sums.
	foreach my $plan_entry (@plan) {
		print STDERR "Computing MD5 of $plan_entry->{filename}: ";
		run_md5_file($plan_entry, sub { my $progress = shift; print STDERR "."; } );
		print STDERR " done\n";
	}
	
	# Show the user.
	print_plan_sums(@plan);
}


sub print_plan_sums
{
	my (@plan) = @_;
	foreach my $plan_entry (@plan) {
		next unless $plan_entry->{computed_md5};
		
		my $sumcolour = "cyan";
		if ($plan_entry->{correct_md5}) {
			$sumcolour = ( $plan_entry->{computed_md5} eq $plan_entry->{correct_md5} ) ? "green" : "red";
		}
		
		print colored($plan_entry->{computed_md5}, $sumcolour) . "  " . $plan_entry->{filename}, "\n";
	}
	
	#### TODO: Show files with errors. Separate sub?
}