sub create_file_sums
{
	my ($md5sum_filename, @dirs_and_files_to_sum) = @_;
	
	# Initialise our plan from the files and directories given on the commandline.
	# Traversing a big tree recursively could take a while, so let the user know.
	print STDERR colored("Constructing plan...", 'yellow'), "\n";
	my @plan;
	find(	{
				# We pass in an anonymous sub to File::Find for it to run on every file and directory
				# as it traverses the tree. Note that the parameter name, 'wanted', is a misnomer;
				# File::Find will not do anything with the return value of our sub.
				wanted => sub {
					# Apparently File::Find doesn't handle utf8 filenames.
					my $fullpathname = $File::Find::name;
					utf8::decode($fullpathname);
					
					if (-e $fullpathname && ! -d $fullpathname) {
						push @plan, create_plan_for_filename($fullpathname);
					}
				},
				follow_skip => 2,		# If we get duplicate dirs, files etc. during find, ignore and carry on.
				no_chdir => 1,			# We want our current dir consistent after Find does its work.
			},
			@dirs_and_files_to_sum
		);
	@plan = prune_plan_duplicates(@plan);
	print STDERR colored("Complete. Plan contains " . scalar @plan . " items.", 'yellow'), "\n";
	
	# Go sum all those files.
	run_md5_files_with_progress("Creating MD5", @plan);
	
	# Then write the computed MD5s to the md5sum file given with --create.
	save_md5sum_file($md5sum_filename, @plan);
}


sub prune_plan_duplicates
{
	my (@plan) = @_;
	my @uniq_plan;
	my %names;
	foreach my $plan_entry (@plan) {
		next unless $plan_entry->{filename};
		if ($names{$plan_entry->{filename}} // 0 == 1) {
			print STDERR colored("Warning: File '$plan_entry->{filename}' mentioned multiple times. Duplicate entries in plan will be ignored.", 'bold yellow'), "\n";
		}
		push @uniq_plan, $plan_entry unless $names{$plan_entry->{filename}};
		$names{$plan_entry->{filename}}++;
	}
	return @uniq_plan;
}