#!/usr/bin/perl -CSDA

use strict;
use warnings;
use utf8;
use File::Find;

find(	{
			wanted => sub {
					my $n = $File::Find::name;
					utf8::decode($n);
					print "$n";
					if (-e $n && ! -d $n) {
						print " - will checksum\n";
					} else {
						print "\n";
					}
				},
			no_chdir => 1
		}, ".");
