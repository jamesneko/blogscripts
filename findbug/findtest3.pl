#!/usr/bin/perl -CSDA

use strict;
use warnings;
use utf8;
use File::Find;

print "\@ARGV is ", join(', ', @ARGV), " and ";
print utf8::is_utf8($ARGV[0])? "has" : "does not have";
print " the utf8 flag set.\n";

my $dir = ".";
print "My \$dir variable is '$dir' and ";
print utf8::is_utf8($dir)? "has" : "does not have";
print " the utf8 flag set.\n";

my $otherdir = "中文";
print "My \$otherdir variable is '$otherdir' and ";
print utf8::is_utf8($otherdir)? "has" : "does not have";
print " the utf8 flag set.\n";

find(	{
			wanted => sub {
					my $n = $File::Find::name;
					utf8::decode($n);
					print "$n";
					if (-e $n && ! -d $n) {
						print " - will checksum\n";
					} else {
						print "\n";
					}
				},
			no_chdir => 1
		}, @ARGV);
