sub reorder_plan
{
	my @plan = @_;
	my %mapping = @_;
	my @new_plan;
	my $frustration = 0;
	# Go through the plan, pushing pairs on to @new_plan only if it's safe to do that step.
	# It is safe to do that step only if the destination name does not appear in 
	# @plan's remaining list of source names. (or if filename is unchanged)
	while (@plan) {
		my $orig_name = shift @plan;
		my $new_name = shift @plan;
		if ($orig_name eq $new_name || ! defined $mapping{$new_name}) {
			# This one is safe. Add it to the new plan.
			push @new_plan, $orig_name, $new_name;
			delete $mapping{$orig_name};
			$frustration = 0;
		} else {
			# This one is not safe to do (yet). Put it at the back of the current plan.
			push @plan, $orig_name, $new_name;
			$frustration++;
			# Is the whole thing impossible?
			if ($frustration * 2 >= @plan) {
				die "I can't figure out how to order this plan without clobbering things mid-process!\n";
			}
		}
	}
	
	return @new_plan;
}
