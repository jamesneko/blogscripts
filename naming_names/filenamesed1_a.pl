if (@ARGV < 2) {
	print "Usage: $0 '<substitutions>' filename(s)\n";
	print "  e.g. $0 's/from/to/g;' myfiles/*\n";
	exit 1;
}
