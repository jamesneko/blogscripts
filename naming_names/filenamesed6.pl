#!/usr/bin/perl -CSDA

use strict;
use warnings;
use Getopt::Long;

my $dryrun = 0;
GetOptions("dry-run|dryrun" => \$dryrun);

sub help
{
	print "Usage: $0 [--dry-run] '<substitutions>' filename(s)\n";
	print "  e.g. $0 's/from/to/g;' myfiles/*\n";
	exit 1;
}


sub main
{
	my $substitutions = shift @_;
	my @filenames = @_;
	
	my %filename_mapping;    # original name => new name
	my %reverse_mapping;     # new name => original name
	foreach my $orig_filename (@filenames) {
		my $new_filename = transform_filename($substitutions, $orig_filename);
		$filename_mapping{$orig_filename} = $new_filename;
		$reverse_mapping{$new_filename} = $orig_filename;
	}
	
	# Check there are no filename collisions; the number of unique output filenames
	# should be equal to the number of unique input filenames.
	if (keys %filename_mapping != keys %reverse_mapping) {
		die "Given substitution commands result in non-unique filenames, refusing to clobber your files.\n";
	}
	
	my @plan = map { $_, $filename_mapping{$_} } @filenames;	# pairs of oldname, newname.
	# Re-order the plan to make it safe against stepping on our own toes while renaming.
	@plan = reorder_plan(@plan);
	# Show it or do it.
	if ($dryrun) {
		show_plan(@plan);
	} else {
		execute_plan(@plan);
	}
}


sub show_plan
{
	my @plan = @_;
	print "Proposed changes:-\n";
	while (@plan) {
		my $orig_name = shift @plan;
		my $new_name = shift @plan;
		if ($orig_name ne $new_name) {
			print "$orig_name -> $new_name\n";
		} else {
			print "$orig_name : unchanged.\n";
		}
	}
}


sub reorder_plan
{
	my @plan = @_;
	my %mapping = @_;
	my @new_plan;
	my $frustration = 0;
	# Go through the plan, pushing pairs on to @new_plan only if it's safe to do that step.
	# It is safe to do that step only if the destination name does not appear in 
	# @plan's remaining list of source names. (or if filename is unchanged)
	while (@plan) {
		my $orig_name = shift @plan;
		my $new_name = shift @plan;
		if ($orig_name eq $new_name || ! defined $mapping{$new_name}) {
			# This one is safe. Add it to the new plan.
			push @new_plan, $orig_name, $new_name;
			delete $mapping{$orig_name};
			$frustration = 0;
		} else {
			# This one is not safe to do (yet). Put it at the back of the current plan.
			push @plan, $orig_name, $new_name;
			$frustration++;
			# Is the whole thing impossible?
			if ($frustration * 2 >= @plan) {
				die "I can't figure out how to order this plan without clobbering things mid-process!\n";
			}
		}
	}
	
	return @new_plan;
}


sub execute_plan
{
	my @plan = @_;
	print "Renaming files:-\n";
	while (@plan) {
		my $orig_name = shift @plan;
		my $new_name = shift @plan;
		if ($orig_name ne $new_name) {
			print "$orig_name -> $new_name\n";
			rename $orig_name, $new_name or die "Failed to rename $orig_name : $!\n";
		} else {
			print "$orig_name : unchanged.\n";
		}
	}
}


sub transform_filename
{
	my ($substitutions, $orig_filename) = @_;
	
	$_ = $orig_filename;
	my $rval = eval $substitutions;
	my $new_filename = $_;
	die "Error in substitutions: $@" if $@;
	
	return $new_filename;
}


help() if (@ARGV < 2);
main(@ARGV);
