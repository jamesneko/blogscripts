function filenamesed () {
	if [ "$1" = "" -o "$2" = "" ]; then
		echo "Usage: filenamesed 'sedcmds' filename(s)"
		return
	fi
	SEDCMD="$1"
	shift
	while [ "$1" != "" ]; do
		F="$1"
		NF=`echo "$F" | sed "$SEDCMD"`
		if [ "$F" != "$NF" ]; then
			echo "$F -> $NF"
			mv -i "$F" "$NF"
		fi
		shift
	done
}
