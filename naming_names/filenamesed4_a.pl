sub main
{
	my $substitutions = shift @_;
	my @filenames = @_;
	
	my %filename_mapping;    # original name => new name
	my %reverse_mapping;     # new name => original name
	foreach my $orig_filename (@filenames) {
		my $new_filename = transform_filename($substitutions, $orig_filename);
		$filename_mapping{$orig_filename} = $new_filename;
		$reverse_mapping{$new_filename} = $orig_filename;
	}
	
	# Check there are no filename collisions; the number of unique output filenames
	# should be equal to the number of unique input filenames.
	if (keys %filename_mapping != keys %reverse_mapping) {
		die "Given substitution commands result in non-unique filenames, refusing to clobber your files.\n";
	}
	
	show_plan(%filename_mapping);
}
