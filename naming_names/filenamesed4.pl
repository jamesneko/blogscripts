#!/usr/bin/perl -CSDA

use strict;
use warnings;

sub help
{
	print "Usage: $0 '<substitutions>' filename(s)\n";
	print "  e.g. $0 's/from/to/g;' myfiles/*\n";
	exit 1;
}


sub main
{
	my $substitutions = shift @_;
	my @filenames = @_;
	
	my %filename_mapping;    # original name => new name
	my %reverse_mapping;     # new name => original name
	foreach my $orig_filename (@filenames) {
		my $new_filename = transform_filename($substitutions, $orig_filename);
		$filename_mapping{$orig_filename} = $new_filename;
		$reverse_mapping{$new_filename} = $orig_filename;
	}
	
	# Check there are no filename collisions; the number of unique output filenames
	# should be equal to the number of unique input filenames.
	if (keys %filename_mapping != keys %reverse_mapping) {
		die "Given substitution commands result in non-unique filenames, refusing to clobber your files.\n";
	}
	
	show_plan(map { $_, $filename_mapping{$_} } @filenames);
}


sub show_plan
{
	my @plan = @_;
	while (@plan) {
		my $orig_name = shift @plan;
		my $new_name = shift @plan;
		if ($orig_name ne $new_name) {
			print "$orig_name -> $new_name\n";
		} else {
			print "$orig_name : unchanged.\n";
		}
	}
}


sub transform_filename
{
	my ($substitutions, $orig_filename) = @_;
	
	$_ = $orig_filename;
	my $rval = eval $substitutions;
	my $new_filename = $_;
	die "Error in substitutions: $@" if $@;
	
	return $new_filename;
}


help() if (@ARGV < 2);
main(@ARGV);
