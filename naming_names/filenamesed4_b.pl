sub show_plan
{
	my @plan = @_;
	while (@plan) {
		my $orig_name = shift @plan;
		my $new_name = shift @plan;
		if ($orig_name ne $new_name) {
			print "$orig_name -> $new_name\n";
		} else {
			print "$orig_name : unchanged.\n";
		}
	}
}
