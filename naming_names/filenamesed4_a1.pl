sub main
{
	my $substitutions = shift @_;
	my @filenames = @_;
	
	my %filename_mapping;    # original name => new name
	my %reverse_mapping;     # new name => original name
	foreach my $orig_filename (@filenames) {
		my $new_filename = transform_filename($substitutions, $orig_filename);
		$filename_mapping{$orig_filename} = $new_filename;
		$reverse_mapping{$new_filename} = $orig_filename;
	}
