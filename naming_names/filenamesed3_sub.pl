sub transform_filename
{
	my ($substitutions, $orig_filename) = @_;
	
	$_ = $orig_filename;
	my $rval = eval $substitutions;
	my $new_filename = $_;
	die "Error in substitutions: $@" if $@;
	
	return $new_filename;
}
