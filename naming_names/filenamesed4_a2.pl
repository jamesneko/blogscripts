	
	# Check there are no filename collisions; the number of unique output filenames
	# should be equal to the number of unique input filenames.
	if (keys %filename_mapping != keys %reverse_mapping) {
		die "Given substitution commands result in non-unique filenames, refusing to clobber your files.\n";
	}
	
	show_plan(%filename_mapping);
}
