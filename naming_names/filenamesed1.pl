#!/usr/bin/env perl

use strict;
use warnings;

if (@ARGV < 2) {
	print "Usage: $0 '<substitutions>' filename(s)\n";
	print "  e.g. $0 's/from/to/g;' myfiles/*\n";
	exit 1;
}

my $substitutions = shift @ARGV;
foreach my $orig_filename (@ARGV) {
	$_ = $orig_filename;
	my $rval = eval $substitutions;
	my $new_filename = $_;
	die "Error in substitutions: $@" if $@;
	
	if ($new_filename ne $orig_filename) {
		print "$orig_filename -> $new_filename\n";
	} else {
		print "$orig_filename : unchanged.\n";
	}
}

