#!/usr/bin/perl -CSDA

use strict;
use warnings;

sub help
{
	print "Usage: $0 '<substitutions>' filename(s)\n";
	print "  e.g. $0 's/from/to/g;' myfiles/*\n";
	exit 1;
}


sub main
{
	my $substitutions = shift @_;
	foreach my $orig_filename (@_) {
		my $new_filename = transform_filename($substitutions, $orig_filename);
	
		if ($new_filename ne $orig_filename) {
			print "$orig_filename -> $new_filename\n";
		} else {
			print "$orig_filename : unchanged.\n";
		}
	}
}


sub transform_filename
{
	my ($substitutions, $orig_filename) = @_;
	
	$_ = $orig_filename;
	my $rval = eval $substitutions;
	my $new_filename = $_;
	die "Error in substitutions: $@" if $@;
	
	return $new_filename;
}


help() if (@ARGV < 2);
main(@ARGV);
