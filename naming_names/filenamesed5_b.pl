	my @plan = map { $_, $filename_mapping{$_} } @filenames;	# pairs of oldname, newname.
	if ($dryrun) {
		show_plan(@plan);
	} else {
		execute_plan(@plan);
	}
}


sub show_plan
{
	my @plan = @_;
	print "Proposed changes:-\n";
	while (@plan) {
		my $orig_name = shift @plan;
		my $new_name = shift @plan;
		if ($orig_name ne $new_name) {
			print "$orig_name -> $new_name\n";
		} else {
			print "$orig_name : unchanged.\n";
		}
	}
}


sub execute_plan
{
	my @plan = @_;
	print "Renaming files:-\n";
	while (@plan) {
		my $orig_name = shift @plan;
		my $new_name = shift @plan;
		if ($orig_name ne $new_name) {
			print "$orig_name -> $new_name\n";
			rename $orig_name, $new_name or die "Failed to rename $orig_name : $!\n";
		} else {
			print "$orig_name : unchanged.\n";
		}
	}
}
