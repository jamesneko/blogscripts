sub main
{
	# We accept exactly one argument not covered by Getopt::Long : which dir to operate on.
	my $dir = shift @ARGV;
	die "Need to supply a directory name!" unless (defined $dir && -d $dir);
	die "Need to supply a --size!" unless (defined $opts{size});
	die "--move option needs a directory to move to!" if ($opts{move} && ! -d $opts{move});
		
	# Adjust options based on command line.
	my $size = parse_size($opts{size});
	$TIME_ATTR = "atime" if ($opts{atime});
	$TIME_ATTR = "mtime" if ($opts{mtime});
	$TIME_ATTR = "ctime" if ($opts{ctime});
	$TIME_ATTR = "basename" if ($opts{basename});
	$TIME_ATTR = "pathname" if ($opts{pathname});
	
	# Collate file info. This fills in @FILES.
	find({ wanted => \&find_callback, no_chdir => 1 }, $dir);
	
	# Sort by mtime (or whatever was set in $TIME_ATTR)
	my @sorted_files = reverse sort by_time_attr @FILES;	# highest mtime first (oldest last)
	
	# Make a cut if you go above the limit.
	my ($keep_files, $cull_files) = split_files_after_limit($size, @sorted_files);
		
	print "Keeping ", scalar @$keep_files, " files:-\n" if ($opts{verbose});
	print_files(@$keep_files) if ($opts{verbose});
	
	if ($opts{move}) {
		print "\nMoving ", scalar @$cull_files, " files:-\n" if ($opts{verbose});
		print_files(@$cull_files) if ($opts{verbose});
		move_files(@$cull_files);
		
	} elsif ($opts{delete}) {
		print "\nDeleting ", scalar @$cull_files, " files:-\n" if ($opts{verbose});
		print_files(@$cull_files) if ($opts{verbose});
		delete_files(@$cull_files);
	
	} else {
		print "\nWould Cull ", scalar @$cull_files, " files:-\n" if ($opts{verbose});
		print_files(@$cull_files) if ($opts{verbose});

	}
}
