sub find_callback
{
	# We want to use File::Find's no_chdir option, so we want to figure out the basename by ourselves.
	my $pathname = $File::Find::name;
	utf8::decode($pathname);
	my $basename = basename($pathname);
	
	# Don't match dotfiles.
	return if ($basename =~ /^\./);
	# We only care about files, not dirs.
	return unless (-f $pathname);
	
	# Collect stats.
	my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = lstat($pathname);
	push @FILES, {
				pathname => $pathname,
				basename => $basename,
				size => $size,
				atime => $atime,
				mtime => $mtime,
				ctime => $ctime,
			};
}


sub by_time_attr
{
	if ($TIME_ATTR eq 'basename' || $TIME_ATTR eq 'pathname') {
		$a->{$TIME_ATTR} cmp $b->{$TIME_ATTR};
	} else {
		$a->{$TIME_ATTR} <=> $b->{$TIME_ATTR};
	}
}