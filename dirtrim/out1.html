<h2>Decimating Directories</h2>

<p>Whenever you set up some automated system that produces files, there's always that nagging fear that you'll forget about it and it will run rampant, filling up your hard drive with clutter. One good example is using <code>motion</code> as a security system - you want to keep the most recent video clips in case you need to refer back to them, but there's little point in keeping the oldest ones.</p>

<p>Keeping only the most recent <code>n</code> videos and deleting the rest could be problematic, because the individual files could be large. Keeping anything younger than a certain number of days is no good, because there could be a burst of activity that creates a lot of files. So we want to make a script that will trim a directory of files down to a specific <em>size</em>.</p>

<p>This'll be a short one. No need to overthink things, no grand architecture, let's just make a script to trim a directory down to size. Skipping on past the usual boilerplate (You can download the complete script later if you really want a look), let's flesh out the options we want this utility to offer:-</p>

<pre style="color:#000000; background-color:#e0eaee; font-size:10pt; font-family:'Courier New';"><span style="color:#000000; font-weight:bold">my</span> <span style="color:#0057ae">%opts</span><span style="color:#000000">;</span>
GetOptions<span style="color:#000000">(</span>\<span style="color:#0057ae">%opts</span><span style="color:#000000">,</span> <span style="color:#bf0303">'help!'</span><span style="color:#000000">,</span> <span style="color:#bf0303">'delete!'</span><span style="color:#000000">,</span> <span style="color:#bf0303">'move=s'</span><span style="color:#000000">,</span> <span style="color:#bf0303">'size=s'</span><span style="color:#000000">,</span> <span style="color:#bf0303">'atime!'</span><span style="color:#000000">,</span> <span style="color:#bf0303">'mtime!'</span><span style="color:#000000">,</span> <span style="color:#bf0303">'ctime!'</span><span style="color:#000000">,</span> <span style="color:#bf0303">'basename!'</span><span style="color:#000000">,</span> <span style="color:#bf0303">'pathname!'</span><span style="color:#000000">,</span> <span style="color:#bf0303">'verbose!'</span><span style="color:#000000">);</span>

<span style="color:#000000; font-weight:bold">sub</span> help
<span style="color:#000000">{</span>
   <span style="color:#000000; font-weight:bold">print</span> STDERR <span style="color:#000000">&lt;&lt;</span>EOF<span style="color:#000000">;</span>
Usage<span style="color:#000000">:</span> <span style="color:#0057ae">$0</span> <span style="color:#000000">--</span>size <span style="color:#000000">&lt;</span>size<span style="color:#000000">&gt;{</span>K<span style="color:#000000">,</span>M<span style="color:#000000">,</span>G<span style="color:#000000">}</span> dirname<span style="color:#000000">/ --</span><span style="color:#000000; font-weight:bold">delete</span>
       <span style="color:#0057ae">$0</span> <span style="color:#000000">--</span>size <span style="color:#000000">&lt;</span>size<span style="color:#000000">&gt;{</span>K<span style="color:#000000">,</span>M<span style="color:#000000">,</span>G<span style="color:#000000">}</span> dirname<span style="color:#010181">/ --move destdir/</span>

       Omitting <span style="color:#000000">--</span><span style="color:#000000; font-weight:bold">delete and</span> <span style="color:#000000">--</span>move does a dry<span style="color:#000000">-</span>run<span style="color:#000000">.</span>

       <span style="color:#000000">--</span>atime    <span style="color:#000000">:</span> Sort by <span style="color:#000000; font-weight:bold">last</span> access <span style="color:#000000; font-weight:bold">time</span>
       <span style="color:#000000">--</span>mtime    <span style="color:#000000">:</span> Sort by <span style="color:#000000; font-weight:bold">last</span> modified <span style="color:#000000; font-weight:bold">time</span> <span style="color:#000000">(</span><span style="color:#000000; font-weight:bold">default</span><span style="color:#000000">)</span>
       <span style="color:#000000">--</span>ctime    <span style="color:#000000">:</span> Sort by <span style="color:#000000; font-weight:bold">last</span> inode change <span style="color:#000000; font-weight:bold">time</span>
       <span style="color:#000000">--</span>basename <span style="color:#000000">:</span> Sort by file basename
       <span style="color:#000000">--</span>pathname <span style="color:#000000">:</span> Sort by file pathname

       <span style="color:#000000">--</span>verbose <span style="color:#000000">:</span> Show the list of files with timestamps

Sorts files contained in the <span style="color:#000000; font-weight:bold">given</span> directory according to age<span style="color:#000000">,</span> <span style="color:#000000; font-weight:bold">and</span> deletes
the oldest files <span style="color:#000000; font-weight:bold">until</span> the total size of the directory is under the <span style="color:#000000; font-weight:bold">given</span>
size limit<span style="color:#000000">.</span>
EOF
   <span style="color:#000000; font-weight:bold">return</span> <span style="color:#b07e00">1</span><span style="color:#000000">;</span>
<span style="color:#000000">}</span>
</pre>

<p>Since it's designed to run in an automated fashion, it's not going to print any output by default, and it's not going to take any action either, unless explicitly specified by <code>--delete</code> or <code>--move</code>. Sorting files by their "mtime" is the most likely use-case, so it's the default, but I do want to leave the door open for other sorting options in case I need them.</p>

<p>On to the <code>main()</code> subroutine. I'm going to be using two 'global' variables, variables declared outside the lexical scope of any of these subroutines which can be accessed by any of them. <code>@FILES</code> will be our list of hash-refs, each one representing a file we are potentially culling and containing keys storing the file name, size, and modification times. <code>$TIME_ATTR</code> just contains the name of the attribute we want to sort by. Generally speaking, it's good programming practice to avoid magic globals that get modified via spooky-action-at-a-distance. However, this is a small script and the alternative would be to generate our collating and sorting functions in a very roundabout way using closures. We don't really need that complexity for a simple little script.</p>

<p>The <code>main()</code> sub needs to check over the command-line arguments, then use <a href="http://metacpan.org/pod/File::Find">File::Find</a> to populate <code>@FILES</code>. Rather than write the entire <code>find</code> callback in-line, this time I feel it's big enough to put in its own subroutine. The second main step that needs to happen is sorting <code>@FILES</code> by the chosen attribute - I'm using the variant of Perl's builtin <a href="http://perldoc.perl.org/functions/sort.html">sort</a> that allows you to specify the name of a subroutine to use to do the sorting.</p>

<pre style="color:#000000; background-color:#e0eaee; font-size:10pt; font-family:'Courier New';"><span style="color:#000000; font-weight:bold">sub</span> main
<span style="color:#000000">{</span>
   <span style="color:#838183; font-style:italic"># We accept exactly one argument not covered by Getopt::Long : which dir to operate on.</span>
   <span style="color:#000000; font-weight:bold">my</span> <span style="color:#0057ae">$dir</span> <span style="color:#000000">=</span> <span style="color:#000000; font-weight:bold">shift</span> <span style="color:#0057ae">&#64;ARGV</span><span style="color:#000000">;</span>
   <span style="color:#000000; font-weight:bold">die</span> <span style="color:#bf0303">&quot;Need to supply a directory name!&quot;</span> <span style="color:#000000; font-weight:bold">unless</span> <span style="color:#000000">(</span><span style="color:#000000; font-weight:bold">defined</span> <span style="color:#0057ae">$dir</span> <span style="color:#000000">&amp;&amp; -</span>d <span style="color:#0057ae">$dir</span><span style="color:#000000">);</span>
   <span style="color:#000000; font-weight:bold">die</span> <span style="color:#bf0303">&quot;Need to supply a --size!&quot;</span> <span style="color:#000000; font-weight:bold">unless</span> <span style="color:#000000">(</span><span style="color:#000000; font-weight:bold">defined</span> <span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>size<span style="color:#000000">});</span>
   <span style="color:#000000; font-weight:bold">die</span> <span style="color:#bf0303">&quot;--move option needs a directory to move to!&quot;</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>move<span style="color:#000000">} &amp;&amp; ! -</span>d <span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>move<span style="color:#000000">});</span>

   <span style="color:#838183; font-style:italic"># Adjust options based on command line.</span>
   <span style="color:#000000; font-weight:bold">my</span> <span style="color:#0057ae">$size</span> <span style="color:#000000">=</span> parse_size<span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>size<span style="color:#000000">});</span>
   <span style="color:#0057ae">$TIME_ATTR</span> <span style="color:#000000">=</span> <span style="color:#bf0303">&quot;atime&quot;</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>atime<span style="color:#000000">});</span>
   <span style="color:#0057ae">$TIME_ATTR</span> <span style="color:#000000">=</span> <span style="color:#bf0303">&quot;mtime&quot;</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>mtime<span style="color:#000000">});</span>
   <span style="color:#0057ae">$TIME_ATTR</span> <span style="color:#000000">=</span> <span style="color:#bf0303">&quot;ctime&quot;</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>ctime<span style="color:#000000">});</span>
   <span style="color:#0057ae">$TIME_ATTR</span> <span style="color:#000000">=</span> <span style="color:#bf0303">&quot;basename&quot;</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>basename<span style="color:#000000">});</span>
   <span style="color:#0057ae">$TIME_ATTR</span> <span style="color:#000000">=</span> <span style="color:#bf0303">&quot;pathname&quot;</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>pathname<span style="color:#000000">});</span>

   <span style="color:#838183; font-style:italic"># Collate file info. This fills in &#64;FILES.</span>
   find<span style="color:#000000">({</span> wanted <span style="color:#000000">=&gt;</span> \<span style="color:#000000">&amp;</span>find_callback<span style="color:#000000">,</span> no_chdir <span style="color:#000000">=&gt;</span> <span style="color:#b07e00">1</span> <span style="color:#000000">},</span> <span style="color:#0057ae">$dir</span><span style="color:#000000">);</span>

   <span style="color:#838183; font-style:italic"># Sort by mtime (or whatever was set in $TIME_ATTR)</span>
   <span style="color:#000000; font-weight:bold">my</span> <span style="color:#0057ae">&#64;sorted_files</span> <span style="color:#000000">=</span> <span style="color:#000000; font-weight:bold">reverse sort</span> by_time_attr <span style="color:#0057ae">&#64;FILES</span><span style="color:#000000">;</span>  <span style="color:#838183; font-style:italic"># highest mtime first (oldest last)</span>

   <span style="color:#838183; font-style:italic"># Make a cut if you go above the limit.</span>
   <span style="color:#000000; font-weight:bold">my</span> <span style="color:#000000">(</span><span style="color:#0057ae">$keep_files</span><span style="color:#000000">,</span> <span style="color:#0057ae">$cull_files</span><span style="color:#000000">) =</span> split_files_after_limit<span style="color:#000000">(</span><span style="color:#0057ae">$size</span><span style="color:#000000">,</span> <span style="color:#0057ae">&#64;sorted_files</span><span style="color:#000000">);</span>

   <span style="color:#000000; font-weight:bold">print</span> <span style="color:#bf0303">&quot;Keeping &quot;</span><span style="color:#000000">,</span> <span style="color:#000000; font-weight:bold">scalar</span> &#64;<span style="color:#0057ae">$keep_files</span><span style="color:#000000">,</span> <span style="color:#bf0303">&quot; files:-</span><span style="color:#ff00ff">\n</span><span style="color:#bf0303">&quot;</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>verbose<span style="color:#000000">});</span>
   print_files<span style="color:#000000">(</span>&#64;<span style="color:#0057ae">$keep_files</span><span style="color:#000000">)</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>verbose<span style="color:#000000">});</span>

   <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>move<span style="color:#000000">}) {</span>
      <span style="color:#000000; font-weight:bold">print</span> <span style="color:#bf0303">&quot;</span><span style="color:#ff00ff">\n</span><span style="color:#bf0303">Moving &quot;</span><span style="color:#000000">,</span> <span style="color:#000000; font-weight:bold">scalar</span> &#64;<span style="color:#0057ae">$cull_files</span><span style="color:#000000">,</span> <span style="color:#bf0303">&quot; files:-</span><span style="color:#ff00ff">\n</span><span style="color:#bf0303">&quot;</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>verbose<span style="color:#000000">});</span>
      print_files<span style="color:#000000">(</span>&#64;<span style="color:#0057ae">$cull_files</span><span style="color:#000000">)</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>verbose<span style="color:#000000">});</span>
      move_files<span style="color:#000000">(</span>&#64;<span style="color:#0057ae">$cull_files</span><span style="color:#000000">);</span>

   <span style="color:#000000">}</span> <span style="color:#000000; font-weight:bold">elsif</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span><span style="color:#000000; font-weight:bold">delete</span><span style="color:#000000">}) {</span>
      <span style="color:#000000; font-weight:bold">print</span> <span style="color:#bf0303">&quot;</span><span style="color:#ff00ff">\n</span><span style="color:#bf0303">Deleting &quot;</span><span style="color:#000000">,</span> <span style="color:#000000; font-weight:bold">scalar</span> &#64;<span style="color:#0057ae">$cull_files</span><span style="color:#000000">,</span> <span style="color:#bf0303">&quot; files:-</span><span style="color:#ff00ff">\n</span><span style="color:#bf0303">&quot;</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>verbose<span style="color:#000000">});</span>
      print_files<span style="color:#000000">(</span>&#64;<span style="color:#0057ae">$cull_files</span><span style="color:#000000">)</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>verbose<span style="color:#000000">});</span>
      delete_files<span style="color:#000000">(</span>&#64;<span style="color:#0057ae">$cull_files</span><span style="color:#000000">);</span>

   <span style="color:#000000">}</span> <span style="color:#000000; font-weight:bold">else</span> <span style="color:#000000">{</span>
      <span style="color:#000000; font-weight:bold">print</span> <span style="color:#bf0303">&quot;</span><span style="color:#ff00ff">\n</span><span style="color:#bf0303">Would Cull &quot;</span><span style="color:#000000">,</span> <span style="color:#000000; font-weight:bold">scalar</span> &#64;<span style="color:#0057ae">$cull_files</span><span style="color:#000000">,</span> <span style="color:#bf0303">&quot; files:-</span><span style="color:#ff00ff">\n</span><span style="color:#bf0303">&quot;</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>verbose<span style="color:#000000">});</span>
      print_files<span style="color:#000000">(</span>&#64;<span style="color:#0057ae">$cull_files</span><span style="color:#000000">)</span> <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$opts</span><span style="color:#000000">{</span>verbose<span style="color:#000000">});</span>

   <span style="color:#000000">}</span>
<span style="color:#000000">}</span>
</pre>

<p>We further delegate the actual splitting of the list of files to another subroutine, <code>split_files_after_limit()</code>, and finally act on <code>--delete</code> or <code>--move</code> if they were specified.</p>

<p>So, what do the finding and sorting callbacks look like?</p>

<pre style="color:#000000; background-color:#e0eaee; font-size:10pt; font-family:'Courier New';"><span style="color:#000000; font-weight:bold">sub</span> find_callback
<span style="color:#000000">{</span>
   <span style="color:#838183; font-style:italic"># We want to use File::Find's no_chdir option, so we want to figure out the basename by ourselves.</span>
   <span style="color:#000000; font-weight:bold">my</span> <span style="color:#0057ae">$pathname</span> <span style="color:#000000">=</span> <span style="color:#0057ae">$File</span><span style="color:#000000">::</span>Find<span style="color:#000000">::</span>name<span style="color:#000000">;</span>
   utf8<span style="color:#000000">::</span>decode<span style="color:#000000">(</span><span style="color:#0057ae">$pathname</span><span style="color:#000000">);</span>
   <span style="color:#000000; font-weight:bold">my</span> <span style="color:#0057ae">$basename</span> <span style="color:#000000">=</span> basename<span style="color:#000000">(</span><span style="color:#0057ae">$pathname</span><span style="color:#000000">);</span>

   <span style="color:#838183; font-style:italic"># Don't match dotfiles.</span>
   <span style="color:#000000; font-weight:bold">return if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$basename</span> <span style="color:#000000">=~</span> <span style="color:#010181">/^\./</span><span style="color:#000000">);</span>
   <span style="color:#838183; font-style:italic"># We only care about files, not dirs.</span>
   <span style="color:#000000; font-weight:bold">return unless</span> <span style="color:#000000">(-</span>f <span style="color:#0057ae">$pathname</span><span style="color:#000000">);</span>

   <span style="color:#838183; font-style:italic"># Collect stats.</span>
   <span style="color:#000000; font-weight:bold">my</span> <span style="color:#000000">(</span><span style="color:#0057ae">$dev</span><span style="color:#000000">,</span><span style="color:#0057ae">$ino</span><span style="color:#000000">,</span><span style="color:#0057ae">$mode</span><span style="color:#000000">,</span><span style="color:#0057ae">$nlink</span><span style="color:#000000">,</span><span style="color:#0057ae">$uid</span><span style="color:#000000">,</span><span style="color:#0057ae">$gid</span><span style="color:#000000">,</span><span style="color:#0057ae">$rdev</span><span style="color:#000000">,</span><span style="color:#0057ae">$size</span><span style="color:#000000">,</span><span style="color:#0057ae">$atime</span><span style="color:#000000">,</span><span style="color:#0057ae">$mtime</span><span style="color:#000000">,</span><span style="color:#0057ae">$ctime</span><span style="color:#000000">,</span><span style="color:#0057ae">$blksize</span><span style="color:#000000">,</span><span style="color:#0057ae">$blocks</span><span style="color:#000000">) =</span> <span style="color:#000000; font-weight:bold">lstat</span><span style="color:#000000">(</span><span style="color:#0057ae">$pathname</span><span style="color:#000000">);</span>
   <span style="color:#000000; font-weight:bold">push</span> <span style="color:#0057ae">&#64;FILES</span><span style="color:#000000">, {</span>
            pathname <span style="color:#000000">=&gt;</span> <span style="color:#0057ae">$pathname</span><span style="color:#000000">,</span>
            basename <span style="color:#000000">=&gt;</span> <span style="color:#0057ae">$basename</span><span style="color:#000000">,</span>
            size <span style="color:#000000">=&gt;</span> <span style="color:#0057ae">$size</span><span style="color:#000000">,</span>
            atime <span style="color:#000000">=&gt;</span> <span style="color:#0057ae">$atime</span><span style="color:#000000">,</span>
            mtime <span style="color:#000000">=&gt;</span> <span style="color:#0057ae">$mtime</span><span style="color:#000000">,</span>
            ctime <span style="color:#000000">=&gt;</span> <span style="color:#0057ae">$ctime</span><span style="color:#000000">,</span>
         <span style="color:#000000">};</span>
<span style="color:#000000">}</span>


<span style="color:#000000; font-weight:bold">sub</span> by_time_attr
<span style="color:#000000">{</span>
   <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$TIME_ATTR</span> <span style="color:#000000; font-weight:bold">eq</span> <span style="color:#bf0303">'basename'</span> <span style="color:#000000">||</span> <span style="color:#0057ae">$TIME_ATTR</span> <span style="color:#000000; font-weight:bold">eq</span> <span style="color:#bf0303">'pathname'</span><span style="color:#000000">) {</span>
      <span style="color:#0057ae">$a</span><span style="color:#000000">-&gt;{</span><span style="color:#0057ae">$TIME_ATTR</span><span style="color:#000000">}</span> <span style="color:#000000; font-weight:bold">cmp</span> <span style="color:#0057ae">$b</span><span style="color:#000000">-&gt;{</span><span style="color:#0057ae">$TIME_ATTR</span><span style="color:#000000">};</span>
   <span style="color:#000000">}</span> <span style="color:#000000; font-weight:bold">else</span> <span style="color:#000000">{</span>
      <span style="color:#0057ae">$a</span><span style="color:#000000">-&gt;{</span><span style="color:#0057ae">$TIME_ATTR</span><span style="color:#000000">} &lt;=&gt;</span> <span style="color:#0057ae">$b</span><span style="color:#000000">-&gt;{</span><span style="color:#0057ae">$TIME_ATTR</span><span style="color:#000000">};</span>
   <span style="color:#000000">}</span>
<span style="color:#000000">}</span>
</pre>

<p>I've learned my lesson with File::Find, and make sure to use the <code>no_chdir</code> flag, so we always get the filename complete with path elements. We still need to explicitly decode the filename as utf8 ... brave users of non-utf8 systems can hack the file up themselves, I'm sure. We return early if the file is a hidden 'dot' file or if it's not a normal file. Otherwise, we record the information about the file that we're interested in and move on.</p>

<p>Sorting is pretty easy. I'd have done it in-line, but there is one little gotcha: if I wanted to sort by the basename of the file <em>(because sometimes the file names are timestamps with some special meaning, or a sequence number, for example)</em> then I need to compare the sorting arguments with <code>cmp</code>, perl's stringwise comparison operator. Otherwise, we're dealing with a numeric comparison and need to use the <code>&lt;=&gt;</code> numeric comparison operator. See perldoc <a href="http://perldoc.perl.org/perlop.html#Relational-Operators">perlop</a> for more information.</p>

<p>What else? Well, we need to write the subroutine to split a list of files into the two groups, which is straightforward enough; just loop through them and keep a running total of the size.</p>

<pre style="color:#000000; background-color:#e0eaee; font-size:10pt; font-family:'Courier New';"><span style="color:#000000; font-weight:bold">sub</span> split_files_after_limit
<span style="color:#000000">{</span>
   <span style="color:#000000; font-weight:bold">my</span> <span style="color:#000000">(</span><span style="color:#0057ae">$limit</span><span style="color:#000000">,</span> <span style="color:#0057ae">&#64;files</span><span style="color:#000000">) =</span> <span style="color:#0057ae">&#64;_</span><span style="color:#000000">;</span>
   <span style="color:#000000; font-weight:bold">my</span> <span style="color:#000000">(</span><span style="color:#0057ae">&#64;keep</span><span style="color:#000000">,</span> <span style="color:#0057ae">&#64;cull</span><span style="color:#000000">);</span>
   <span style="color:#000000; font-weight:bold">my</span> <span style="color:#0057ae">$running_total</span> <span style="color:#000000">=</span> <span style="color:#b07e00">0</span><span style="color:#000000">;</span>

   <span style="color:#000000; font-weight:bold">foreach</span> <span style="color:#000000; font-weight:bold">my</span> <span style="color:#0057ae">$file</span> <span style="color:#000000">(</span><span style="color:#0057ae">&#64;files</span><span style="color:#000000">) {</span>
      <span style="color:#0057ae">$running_total</span> <span style="color:#000000">+=</span> <span style="color:#0057ae">$file</span><span style="color:#000000">-&gt;{</span>size<span style="color:#000000">};</span>

      <span style="color:#838183; font-style:italic"># If this file is the one that puts us over the limit, it and everything afterwards</span>
      <span style="color:#838183; font-style:italic"># goes in 'cull'.</span>
      <span style="color:#000000; font-weight:bold">if</span> <span style="color:#000000">(</span><span style="color:#0057ae">$running_total</span> <span style="color:#000000">&gt;</span> <span style="color:#0057ae">$limit</span><span style="color:#000000">) {</span>
         <span style="color:#000000; font-weight:bold">push</span> <span style="color:#0057ae">&#64;cull</span><span style="color:#000000">,</span> <span style="color:#0057ae">$file</span><span style="color:#000000">;</span>
      <span style="color:#000000">}</span> <span style="color:#000000; font-weight:bold">else</span> <span style="color:#000000">{</span>
         <span style="color:#000000; font-weight:bold">push</span> <span style="color:#0057ae">&#64;keep</span><span style="color:#000000">,</span> <span style="color:#0057ae">$file</span><span style="color:#000000">;</span>
      <span style="color:#000000">}</span>
   <span style="color:#000000">}</span>

   <span style="color:#000000; font-weight:bold">return</span> <span style="color:#000000">(</span>\<span style="color:#0057ae">&#64;keep</span><span style="color:#000000">,</span> \<span style="color:#0057ae">&#64;cull</span><span style="color:#000000">);</span>
<span style="color:#000000">}</span>
</pre>

<p>Moving the files is easily accomplished with the <a href="http://metacpan.org/pod/File::Copy">File::Copy</a> module, and deleting is accomplished with the builtin <code>unlink</code> function. I hope this script proves useful to people - you can download it on the <a href="http://blog.lazycat.com.au/p/downloads-script-toolbox.html">Script Toolbox</a> downloads page.</p>
