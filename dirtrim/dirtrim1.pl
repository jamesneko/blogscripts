my %opts;
GetOptions(\%opts, 'help!', 'delete!', 'move=s', 'size=s', 'atime!', 'mtime!', 'ctime!', 'basename!', 'pathname!', 'verbose!');

sub help
{
	print STDERR <<EOF;
Usage: $0 --size <size>{K,M,G} dirname/ --delete
       $0 --size <size>{K,M,G} dirname/ --move destdir/
       
       Omitting --delete and --move does a dry-run.

       --atime    : Sort by last access time
       --mtime    : Sort by last modified time (default)
       --ctime    : Sort by last inode change time
       --basename : Sort by file basename
       --pathname : Sort by file pathname
       
       --verbose : Show the list of files with timestamps

Sorts files contained in the given directory according to age, and deletes
the oldest files until the total size of the directory is under the given
size limit.
EOF
	return 1;
}
