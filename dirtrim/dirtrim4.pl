sub split_files_after_limit
{
	my ($limit, @files) = @_;
	my (@keep, @cull);
	my $running_total = 0;
	
	foreach my $file (@files) {
		$running_total += $file->{size};
		
		# If this file is the one that puts us over the limit, it and everything afterwards
		# goes in 'cull'.
		if ($running_total > $limit) {
			push @cull, $file;
		} else {
			push @keep, $file;
		}
	}
	
	return (\@keep, \@cull);
}