Stealing Simulator 2014
-----------------------

The [Spoiler Warning](http://youtube.com/user/SpoilerWarningShow) crew have been playing Skyrim. In typical Elder Scrolls protagonist fashion, Reginald Catbert has been stealing anything that's not nailed down, and this led to the discussion of how characters in the game react to the criminal tendencies of the player. In this case, they take it... poorly.

If you have a rich, immersive RPG where the player can take objects and these objects may not necessairly belong to the player, there are two main extremes that you can take gameplay-wise. 

 * Let the player take whatever they please without repercussions, even selling merchants' own items right back to them.
 * All guards are instantly, psychically aware of witnessed crimes and all merchants can identify stolen goods. All crime is rightly attributed to the player character.
 
 The first approach puts me in mind of many JRPGs - feel free to rummage through people's homes to find a Cure potion or two, no-one will mind because either they're too dim or because you're the legendary hero.
 
 The second approach is more common in Western RPGs. They want to give the player the *option* to roleplay a sneaky thief, but without necessairly devoting their entire game to it. In a sandbox game where you want to please everyone, you can't always get sufficient depth of gameplay for every player.
 
 
