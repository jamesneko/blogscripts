#!/usr/bin/perl

use warnings;
use strict;
use utf8;
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");
use XML::Twig;		# Package Name: libxml-twig-perl
use Data::Dumper;
use FindBin qw/$RealBin/;

my $igoogle_settings_filename = "$RealBin/iGoogle-settings.xml";
print STDERR "Opening iGoogle settings file: $igoogle_settings_filename\n";

my $tabs = {};	# Tab Name => [ urls ];
# Called by XML::Twig on each ModulePrefs part.
sub collect_feed_urls
{
	my ($twig, $element) = @_;
	# Extract the URL from this element, and the tab name from its ancestor.
	my $url = $element->att('xmlUrl');
	my $tab_name = $element->parent('Tab')->att('title');
	# Put feed urls in an array ref, grouped by tab name.
	$tabs->{$tab_name} = [] unless defined $tabs->{$tab_name};
	push @{$tabs->{$tab_name}}, $url;
}

# Create our Twig object and tell it what bits we're interested in.
my $twig = XML::Twig->new(
		twig_handlers => {
			'Tab//Module[@type="RSS"]/ModulePrefs' => \&collect_feed_urls,
		}
	);
$twig->parsefile($igoogle_settings_filename); # XML::Twig will just die if there's a problem. Fine by me.

# Print everything out to check it worked so far.
print STDERR Dumper($tabs);