#!/usr/bin/perl

use warnings;
use strict;
use utf8::all;		# Package Name: libutf8-all-perl
use XML::Twig;		# Package Name: libxml-twig-perl
use XML::RSS;		# Package Name: libxml-rss-perl
use LWP::UserAgent;
use FindBin qw/$RealBin/;

my $igoogle_settings_filename = "$RealBin/iGoogle-settings.xml";
my $number_of_rss_items_per_feed = 8;
print STDERR "Opening iGoogle settings file: $igoogle_settings_filename\n";

my $tabs = {};	# Tab Name => [ urls ];
# Called by XML::Twig on each ModulePrefs part.
sub collect_feed_urls
{
	my ($twig, $element) = @_;
	# Extract the URL from this element, and the tab name from its ancestor.
	my $url = $element->att('xmlUrl');
	my $tab_name = $element->parent('Tab')->att('title');
	# Put feed urls in an array ref, grouped by tab name.
	$tabs->{$tab_name} = [] unless defined $tabs->{$tab_name};
	push @{$tabs->{$tab_name}}, $url;
}

# Keep a quick record of any error messages that might help debug why a feed failed, for inclusion in the HTML.
my $error_messages = {};	# feed url -> string

sub main
{
	# Create our Twig object and tell it what bits we're interested in.
	my $twig = XML::Twig->new(
			twig_handlers => {
				'Tab//Module[@type="RSS"]/ModulePrefs' => \&collect_feed_urls,
			}
		);
	$twig->parsefile($igoogle_settings_filename); # XML::Twig will just die if there's a problem. Fine by me.

	# Fetch the feeds.
	my $rsses = get_feeds($tabs);

	# Print the HTML.
	print create_html($tabs, $rsses);
}


sub get_feeds
{
	my ($tabs) = @_;
	# Fetch the feeds and build a hashref of feed url => XML:RSS object (or undef)
	my $rsses = {};
	foreach my $url_list (values %$tabs) {
		foreach my $url (@$url_list) {
			$rsses->{$url} = get_feed($url);
		}
	}
	return $rsses;
}

sub get_feed
{
	my ($url) = @_;
	# Use LWP::UserAgent to fetch the page for us, and decode it from whatever text encoding it uses.
	print STDERR "Fetching $url ...";
	my $ua = LWP::UserAgent->new;
	$ua->timeout(30);		# A timeout of 30 seconds seems reasonable.
	$ua->env_proxy;		# Read proxy settings from environment variables.
	# One step that became necessary: LIE TO THE WEBSITES. Without this, Questionable Content
	# and some other sites instantly give us a 403 Forbidden when we try to get their RSS.
	# I know that Jeph Jacques, author of QC, did have some problem a while ago with some
	# Android app that was "Stealing" his content by grabbing the image directly and not the
	# advertising that supports his site. That's fine, but user-agent filters harm the web.
	# I'm just trying to get a feed of comic updates, here - I don't care if there's no in-line
	# image right there in the feed, all I need is a simple list of links to QC's archive
	# pages. That way Jeph gets his ad revenue, and his site is easy to check for new updates.
	# Sigh. Stuff like this, and sites that require Javascript to display simple images and text
	# are a blight on the internet.
	$ua->agent("Mozilla/5.0 Firefox/25.0");
	
	my $response = $ua->get($url);
	unless ($response->is_success) {
		print STDERR " Failed! ", $response->status_line, "\n";
		$error_messages->{$url} = "Feed download failed: $response->status_line";
		return;
	}
	my $feed_text = $response->decoded_content;
	
	# Obtained a bit of text over the interwebs, but is it valid RSS?
	my $rss = XML::RSS->new;
	eval {
		$rss->parse($feed_text);
	};
	if ($@) {
		print STDERR " Bad RSS feed!\n";
		$error_messages->{$url} = "RSS parsing failed: $@";
		return;
	}
	
	print STDERR " OK\n";
	return $rss;
}


sub create_html
{
	my ($tabs, $rsses) = @_;
	my $output = "";
	# My HTML syntax-highlighter hates the <meta> line for some reason, but only in the full script.
	# It's getting confused about just what it's being asked to highlight, perhaps.
	# Oh well! TMTOWTDI.
	my $content = "text/html;charset=utf-8";
	$output .= qq{<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">\n};
	$output .= qq!<html>\n!;
	$output .= qq!  <head>\n!;
	$output .= qq!    <meta http-equiv="content-type" content="$content" />\n!;
	$output .= qq!    <title>maiGoogle</title>\n!;
	$output .= qq!  </head>\n!;
	$output .= qq!  <body>\n!;
	
	$output .= html_body($tabs, $rsses);
	
	$output .= qq!  </body>\n!;
	$output .= qq!</html>\n!;
	return $output;
}

sub html_body
{
	my ($tabs, $rsses) = @_;
	my $output = "";
	# For each Tab, print a header and the feeds. Tabs are sorted in alphabetical order, why not.
	foreach my $tabname (sort keys %$tabs) {
		$output .= html_tab($tabname, $tabs->{$tabname}, $rsses);
	}
	return $output;
}

sub html_tab
{
	my ($tabname, $url_list, $rsses) = @_;
	my $output = "";
	$output .= qq!\n<h1>$tabname</h1>\n!;
	foreach my $url (@$url_list) {
		my $rss = $rsses->{$url};
		$output .= html_rss_feed($url, $rss);
	}
	return $output;
}

sub sanify
{
	my ($str) = @_;
	$str //= "";
	$str =~ s/</&lt;/g;
	$str =~ s/>/&gt;/g;
	$str =~ s/&/&amp;/g;
	return $str;
}

sub html_rss_feed
{
	my ($url, $rss) = @_;
	my $output = "";
	
	# Not all the feeds might have been fetched OK, or some might not have parsed properly.
	if ( ! defined $rss) {
		# Do we know what went wrong?
		my $error = $error_messages->{$url} // "The feed failed to load for mysterious reasons, sorry.";
		sanify($error);
		$output .= qq!<div class="feed">\n!;
		$output .= qq!  <span class="title"><a href="$url">$url</a></span>\n!;
		$output .= qq!  <p class="fail">$error</p>\n!;
		$output .= qq!</div>\n!;
		return $output;
	}
	
	# Feed seems to have loaded OK.

	# Figure out what the title of this feed is; default to the URL.
	my $title = $rss->channel('title') // $url;
	$title = sanify($title);
	# Where should clicking the title of the feed box link to?
	my $title_link = $rss->channel('link') // $url;
	$title_link = sanify($title_link);
	
	# Show them the top few items.

	$output .= qq!<div class="feed">\n!;
	$output .= qq!  <span class="title"><a href="$title_link">$title</a></span>\n!;
	$output .= qq!  <span class="rsslink"><a href="$url">rss</a></span>\n!;
	$output .= qq!  <ul>\n!;
	
	my @items = @{$rss->{items}};
	# Remove elements from the list, starting at N, to the end. This leaves elements 0..(N-1).
	splice @items, $number_of_rss_items_per_feed;
	
	# Show them as list items.
	foreach my $item (@items) {
		$output .= qq!    <li> !;
		$output .= html_rss_item($item);
		$output .= qq! </li>\n!;
	}
	$output .= qq!  </ul>\n!;
	$output .= qq!</div>\n!;

	return $output;
}

sub html_rss_item
{
	my ($item) = @_;
	# Return an html link to this RSS feed item.
	my $title = sanify($item->{title});
	my $link = sanify($item->{link});
	return qq!<a href="$link">$title</a>!;
}

main();
