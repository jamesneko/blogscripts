sub sanify
{
	my ($str) = @_;
	my $hs = HTML::Strip->new();
	$str = $hs->parse($str);
	$hs->eof();
	return $str;
}