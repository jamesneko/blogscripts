sub sanify
{
	my ($str) = @_;
	$str //= "";
	$str =~ s/</&lt;/g;
	$str =~ s/>/&gt;/g;
	$str =~ s/&/&amp;/g;
	return $str;
}