sub get_feeds
{
	my ($tabs) = @_;
	# Fetch the feeds and build a hashref of feed url => XML:RSS object (or undef)
	my $rsses = {};
	foreach my $url_list (values %$tabs) {
		foreach my $url (@$url_list) {
			$rsses->{$url} = get_feed($url);
		}
	}
	return $rsses;
}

sub get_feed
{
	my ($url) = @_;
	# Use LWP::Simple to fetch the page for us - returns 'undef' on any failure.
	print STDERR "Fetching $url ...";
	my $feed_text = get($url);
	if ( ! defined $feed_text) {
		print STDERR " Failed!\n";
		return;
	}
	
	# Obtained a bit of text over the interwebs, but is it valid RSS?
	my $rss = XML::RSS->new;
	eval {
		$rss->parse($feed_text);
	};
	if ($@) {
		print STDERR " Bad RSS feed!\n";
		return;
	}
	
	print STDERR " OK\n";
	return $rss;
}