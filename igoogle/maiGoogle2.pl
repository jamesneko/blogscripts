#!/usr/bin/perl

use warnings;
use strict;
use utf8;
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");
use XML::Twig;		# Package Name: libxml-twig-perl
use XML::RSS;		# Package Name: libxml-rss-perl
use LWP::Simple;
use Data::Dumper;
use FindBin qw/$RealBin/;

my $igoogle_settings_filename = "$RealBin/iGoogle-settings.xml";
my $number_of_rss_items_per_feed = 8;
print STDERR "Opening iGoogle settings file: $igoogle_settings_filename\n";

my $tabs = {};	# Tab Name => [ urls ];
# Called by XML::Twig on each ModulePrefs part.
sub collect_feed_urls
{
	my ($twig, $element) = @_;
	# Extract the URL from this element, and the tab name from its ancestor.
	my $url = $element->att('xmlUrl');
	my $tab_name = $element->parent('Tab')->att('title');
	# Put feed urls in an array ref, grouped by tab name.
	$tabs->{$tab_name} = [] unless defined $tabs->{$tab_name};
	push @{$tabs->{$tab_name}}, $url;
}


sub main
{
	# Create our Twig object and tell it what bits we're interested in.
	my $twig = XML::Twig->new(
			twig_handlers => {
				'Tab//Module[@type="RSS"]/ModulePrefs' => \&collect_feed_urls,
			}
		);
	$twig->parsefile($igoogle_settings_filename); # XML::Twig will just die if there's a problem. Fine by me.

	# Fetch the feeds.
	my $rsses = get_feeds($tabs);

	# Print the HTML.
	print create_html($tabs, $rsses);
}


sub get_feeds
{
	my ($tabs) = @_;
	# Fetch the feeds and build a hashref of feed url => XML:RSS object (or undef)
	my $rsses = {};
	foreach my $url_list (values %$tabs) {
		foreach my $url (@$url_list) {
			$rsses->{$url} = get_feed($url);
		}
	}
	return $rsses;
}

sub get_feed
{
	my ($url) = @_;
	# Use LWP::Simple to fetch the page for us - returns 'undef' on any failure.
	print STDERR "Fetching $url ...";
	my $feed_text = get($url);
	if ( ! defined $feed_text) {
		print STDERR " Failed!\n";
		return;
	}
	
	# Obtained a bit of text over the interwebs, but is it valid RSS?
	my $rss = XML::RSS->new;
	eval {
		$rss->parse($feed_text);
	};
	if ($@) {
		print STDERR " Bad RSS feed!\n";
		return;
	}
	
	print STDERR " OK\n";
	return $rss;
}


sub create_html
{
	my ($tabs, $rsses) = @_;
	my $output = "";
	$output .= <<EOF;
		<html>
			<head>
				<title>maiGoogle</title>
			</head>
			<body>
EOF

	$output .= html_body($tabs, $rsses);
	
	$output .= <<EOF;
			</body>
		</html>
EOF
	return $output;
}

sub html_body
{
	my ($tabs, $rsses) = @_;
	my $output = "";
	# For each Tab, print a header and the feeds. Tabs are sorted in alphabetical order, why not.
	foreach my $tabname (sort keys %$tabs) {
		$output .= html_tab($tabname, $tabs->{$tabname}, $rsses);
	}
	return $output;
}

sub html_tab
{
	my ($tabname, $url_list, $rsses) = @_;
	my $output = "";
	$output .= qq!\n<h1>$tabname</h1>\n!;
	foreach my $url (@$url_list) {
		my $rss = $rsses->{$url};
		$output .= html_rss_feed($url, $rss);
	}
	return $output;
}

sub html_rss_feed
{
	my ($url, $rss) = @_;
	my $output = "";
	
	# Not all the feeds might have been fetched OK, or some might not have parsed properly.
	if ( ! defined $rss) {
		# Ideally, we'd remember why, but for now just say sorry.
		$output .= qq!<div class="feed">\n!;
		$output .= qq!  <span class="title"><a href="$url">$url</a></span>\n!;
		$output .= qq!  <p class="fail">This feed failed to load, sorry.</p>\n!;
		$output .= qq!</div>\n!;
		return $output;
	}
	
	# Feed seems to have loaded OK.

	# Figure out what the title of this feed is; default to the URL.
	my $title = $rss->channel('title') // $url;
	# Where should clicking the title of the feed box link to?
	my $title_link = $rss->channel('link') // $url;
	
	# Show them the top few items.

	$output .= qq!<div class="feed">\n!;
	$output .= qq!  <span class="title"><a href="$title_link">$title</a></span>\n!;
	$output .= qq!  <ul>\n!;
	
	my @items = @{$rss->{items}};
	# Remove elements from the list, starting at N, to the end. This leaves elements 0..(N-1).
	splice @items, $number_of_rss_items_per_feed;
	
	# Show them as list items.
	foreach my $item (@items) {
		$output .= qq!    <li> !;
		$output .= html_rss_item($item);
		$output .= qq! </li>\n!;
	}
	$output .= qq!  </ul>\n!;
	$output .= qq!</div>\n!;

	return $output;
}

sub html_rss_item
{
	my ($item) = @_;
	# Return an html link to this RSS feed item.
	my $title = $item->{title};
	my $link = $item->{link};
	return qq!<a href="$link">$title</a>!;
}

main();
