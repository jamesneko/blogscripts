sub html_rss_feed
{
	my ($url, $rss) = @_;
	my $output = "";
	
	# Not all the feeds might have been fetched OK, or some might not have parsed properly.
	if ( ! defined $rss) {
		# Ideally, we'd remember why, but for now just say sorry.
		$output .= qq!<div class="feed">\n!;
		$output .= qq!  <span class="title"><a href="$url">$url</a></span>\n!;
		$output .= qq!  <p class="fail">This feed failed to load, sorry.</p>\n!;
		$output .= qq!</div>\n!;
		return $output;
	}
