#!/usr/bin/perl

# Via http://www.theguardian.com/science/alexs-adventures-in-numberland/2015/may/04/einsteins-election-riddle-are-you-in-the-two-per-cent-that-can-solve-it
#
# I've seen this kind of problem crop up many times, and in puzzle books they often give you a grid,
# mapping names to coffee to pet types and so on, where you can put an X in a grid cell you know for
# certain, and puzzle out the rest from there. It's almost like sudoku, but with a higher
# dimensionality. But rather than work it out with pencil and paper, let's try and do it with code.
#
# There are five houses with the outside walls painted in five different ways. David, Ed, Nick,
# Nicola and Nigel each live in one of the houses. They each drink a certain type of coffee,
# have a preferred mode of transport and keep a certain pet. No owners have the same pet, the
# same preferred mode of transport or drink the same type of coffee.
#
# We then get a bunch of facts tying two or three things together.
#  Nicola lives in the tartan house.
#  Ed has a guinea pig
#  David drinks mochaccino
#  The paisley house is on the left of the gingham house
#  The owner of the paisley house drinks flat whites.
#  The person who drives by car has a squirrel.
#  The owner of the striped house travels by bike.
#  The person living in the centre house drinks double espresso.
#  Nick lives in the first house.
#  The person who travels by train lives next to the one who has a pitbull.
#  The person who has a badger lives next to the person who travels by bike.
#  The person who travels by plane drinks chai latte.
#  Nigel goes everywhere by foot.
#  Nick lives next to the polka dot house.
#  The person who travels by train has a neighbour who drinks decaf.
#
# Q: Who owns the fish?

use strict;
use warnings;
use 5.010;
use Data::Dumper;
use Carp;

# First thing to do is enumerate all the possible values of each aspect.
# Of note is that the houses are the only things where position matters;
# So rather than work out who lives in what house, we'll express things
# in terms of what house contains what human, beverage, travel, colour and pet.
my @NAMES      = qw/DAVID ED NICK NICOLA NIGEL/;
my @DRINKS     = qw/MOCHACCINO FLAT_WHITE DOUBLE_ESPRESSO CHAI_LATTE DECAF/;
my @TRANSPORTS = qw/CAR BIKE TRAIN PLANE FOOT/;
my @COLOURS    = qw/TARTAN PAISLEY GINGHAM STRIPED POLKA_DOT/;
my @PETS       = qw/GUINEA_PIG SQUIRREL PITBULL BADGER FISH/;

# We'll make an array of houses.
# We can initialise each house hash-reference with five attributes,
# and the value of those attributes is an array-reference, containing
# the list of remaining potential values.
my @houses = map {
		{
		   number    => $_,
			name      => [ @NAMES ],
			drink     => [ @DRINKS ],
			transport => [ @TRANSPORTS ],
			colour    => [ @COLOURS ],
			pet       => [ @PETS ],
		};
	} 0..4;

# This expands into a fairly large data structure. We want to now iterate
# over all our facts and treat them as rules we can apply to what we know.
# If it's possible to "cross out" certain bits of information, do so, and
# then go over the rules again to see if that allows us to apply something
# else.

# As part of our rules, one thing we'll be doing a lot is filtering an
# arrayref. Rather than turning it into a list, grepping the list, then
# shoving it back in a new arrayref each time, let's make a helper sub
# to use in the rules.
sub filter(&\$)
{
	my ($coderef, $arrayrefref) = @_;
	my @filtered = grep { &$coderef(); } @$$arrayrefref;
	$$arrayrefref = [ @filtered ];
}

# We also need a nice way to ask if we know something for certain.
# We can pass it a specific house as a hashref, or a list of them.
# It will check all the possible categories, looking for one where
# the given $value is the only possible one in its category.
sub certain($@)
{
	my ($value, @houses) = @_;
	my $possibles = 0;
	foreach my $house (@houses) {
		croak "House '$house' is not a hashref!" unless ref $house eq 'HASH';
		foreach my $category (qw/name drink transport colour pet/) {
			# This house/category has a matching unique value with other possibilities eliminated.
			$possibles++ if @{$house->{$category}} == 1 && $house->{$category}->[0] eq $value;
		}
	}
	# If exactly one house (from those provided) had a matching unique value, we are certain of it.
	return ($possibles == 1);
}

# And for a lot of filter operations, this means also being able to state the negative,
# things which we know aren't so.
sub certainly_not($@)
{
	my ($value, @houses) = @_;
	my $count = 0;
	foreach my $house (@houses) {
		croak "House '$house' is not a hashref!" unless ref $house eq 'HASH';
		foreach my $category (qw/name drink transport colour pet/) {
			# This house/category still has the given value as a possibility; we have not ruled it out yet.
			$count++ if grep { $_ eq $value } @{$house->{$category}};
		}
	}
	# Provided none of the houses supplied have that value, we can be satisfied.
	return ($count == 0);
}

# And then there's the grey area inbetween.
sub possibly($@)
{
	my ($value, @houses) = @_;
	my $count = 0;
	foreach my $house (@houses) {
		croak "House '$house' is not a hashref!" unless ref $house eq 'HASH';
		foreach my $category (qw/name drink transport colour pet/) {
			# This house/category still has the given value as a possibility; we have not ruled it out yet.
			$count++ if grep { $_ eq $value } @{$house->{$category}};
		}
	}
	# If any of the houses supplied have that value one or more times, we can call it possible.
	return ($count > 0);
}


# And be able to ask things about the houses left and right of this one.
sub left(&$)
{
	my ($coderef, $thishouse) = @_;
	croak "House '$thishouse' is not a hashref!" unless ref $thishouse eq 'HASH';
	return if ($thishouse->{number} - 1 < 0);
	$_ = $houses[$thishouse->{number} - 1];
	return &$coderef();
}

sub right(&$)
{
	my ($coderef, $thishouse) = @_;
	croak "House '$thishouse' is not a hashref!" unless ref $thishouse eq 'HASH';
	return if ($thishouse->{number} + 1 >= @houses);
	$_ = $houses[$thishouse->{number} + 1];
	return &$coderef();
}

sub left_or_right(&$)
{
	my ($coderef, $thishouse) = @_;
	return (left { &$coderef() } $thishouse) || (right { &$coderef() } $thishouse);
}

sub left_and_right(&$)
{
	my ($coderef, $thishouse) = @_;
	return (left { &$coderef() } $thishouse) && (right { &$coderef() } $thishouse);
}

sub neither_left_nor_right(&$)
{
	my ($coderef, $thishouse) = @_;
	return ( ! left { &$coderef() } $thishouse) && ( ! right { &$coderef() } $thishouse);
}


# A lot of the rules are expressed as a relationship between two things just within
# the same house, such as "The owner of the paisley house drinks flat whites.".
# These are fairly easy to formulate and we can generalise this nicely.
# Call relationship('colour', 'PAISLEY', 'drink', 'FLAT_WHITE') to generate and
# return subroutine suitable for processing that rule.
sub relationship
{
	my ($category1, $value1, $category2, $value2) = @_;
	# Note how $value1 etc. are captured by the sub below, and will be fixed
	# for future invocations of it. This is a nice thing Perl just does, without
	# any fancy 'lambda' or 'closure' syntax required. Just reference the thing.
	return sub {
			my $house = shift;
			if (certain $value1, $house) {
				filter { $_ eq $value2 } $house->{$category2};
			} elsif (certainly_not $value1, $house) {
				filter { $_ ne $value2 } $house->{$category2};
			}
			if (certain $value2, $house) {
				filter { $_ eq $value1 } $house->{$category1};
			} elsif (certainly_not $value2, $house) {
				filter { $_ ne $value1 } $house->{$category1};
			}
		};
}

# Another common one is a relationship between two neighbours. This isn't often
# directly specified as one being left of the other, and we can only deduce
# things that we know aren't possible due to the lack of a suitable neighbour.
# e.g. neighbour_relationship(pet => 'BADGER', transport => 'BIKE')
sub neighbour_relationship
{
	my ($category1, $value1, $category2, $value2) = @_;
	return sub {
			my $house = shift;
			# We do not know whether A is left of B, or B left of A; so we can't say anything for certain about
			# the houses on the ends simply because they're on the end.
			
			# If both left and right are definitely not badger owners, this house cannot be a bike rider.
			if (neither_left_nor_right { possibly $value1, $_ } $house) {
				filter { $_ ne $value2 } $house->{$category2};
			}
			# If both left and right are not bike riders, this house cannot have a badger.
			if (neither_left_nor_right { possibly $value2, $_ } $house) {
				filter { $_ ne $value1 } $house->{$category1};
			}
		};
}


# We can express the rules as a series of anonymous subs that are applied
# to each house in turn.

my %RULES = (
		"Nick lives in the first house." => sub
		{
			# This is a nice easy one that gives us one of our first concrete facts.
			my $house = shift;
			if ($house->{number} == 0) {
				$house->{name} = [ 'NICK' ];
			}
		},
		"Nick lives next to the polka dot house." => sub
		{
			my $house = shift;
			# First off, we can't determine this rule until we are certain about Nick's location,
			# because we are basically treating it as 'the polka dot house lives next to Nick'.
			return unless certain 'NICK', @houses;
			
			if (certain 'NICK', $house) {
				# Nick's house can't be polka dot.
				filter { $_ ne 'POLKA_DOT' } $house->{colour};
				
			} elsif ( ! left_or_right { certain 'NICK', $_ } $house) {
				# Houses that don't have Nick as a neighbour cannot be polka dot.
				filter { $_ ne 'POLKA_DOT' } $house->{colour};
			}
			# This could also tell us the possible location of Nick, if we knew which the polka dot house was.
			# But since we know that already, there's no need to express it as a rule.
		},
		"The person living in the centre house drinks double espresso." => sub
		{
			my $house = shift;
			filter {
				if ($house->{number} == 2) {
					$_ eq 'DOUBLE_ESPRESSO'
				} else {
					$_ ne 'DOUBLE_ESPRESSO'
				}
			} $house->{drink};
		},
		"Nicola lives in the tartan house." => relationship(name => 'NICOLA', colour => 'TARTAN'),
		"Ed has a guinea pig." => relationship(name => 'ED', pet => 'GUINEA_PIG'),
		"David drinks mochaccino." => relationship(name => 'DAVID', drink => 'MOCHACCINO'),
		"The paisley house is on the left of the gingham house." => sub
		{
			my $house = shift;
			# Also implied: the gingham house cannot be the leftmost house, nor can the paisley house be the rightmost.
			filter { $_ ne 'GINGHAM' } $house->{colour} if $house->{number} == 0;
			filter { $_ ne 'PAISLEY' } $house->{colour} if $house->{number} == 4;
			# if left of this house is certainly not paisley, then we aren't gingham.
			if (left { certainly_not 'PAISLEY', $_ } $house) {
				filter { $_ ne 'GINGHAM' } $house->{colour};
			}
			# if right of this house is certainly not gingham, then we aren't paisley.
			if (right { certainly_not 'GINGHAM', $_ } $house) {
				filter { $_ ne 'PAISLEY' } $house->{colour};
			}
			# Obvious, but implied: The paisley house is not the gingham house.
			# This will be more relevant to encode in other neighbour relationships.
		},
		"The owner of the paisley house drinks flat whites." => relationship(colour => 'PAISLEY', drink => 'FLAT_WHITE'),
		"The person who drives by car has a squirrel." => relationship(transport => 'CAR', pet => 'SQUIRREL'),
		"The owner of the striped house travels by bike." => relationship(colour => 'STRIPED', transport => 'BIKE'),
		"The person who travels by plane drinks chai latte." => relationship(transport => 'PLANE', drink => 'CHAI_LATTE'),
		"Nigel goes everywhere by foot." => relationship(name => 'NIGEL', transport => 'FOOT'),
		"The person who has a badger lives next to the person who travels by bike." => neighbour_relationship(pet => 'BADGER', transport => 'BIKE'),
		"The person who travels by train lives next to the one who has a pitbull." => neighbour_relationship(transport => 'TRAIN', pet => 'PITBULL'),
		"The person who travels by train has a neighbour who drinks decaf." => neighbour_relationship(transport => 'TRAIN', drink => 'DECAF'),
	);


# Show us what we've got so far, in an abbreviated form.
sub possible_values_to_str
{
	my ($values, @positions) = @_;
	my @s;
	for (my $i = 0; $i < @positions; $i++) {
		if (grep { $_ eq $positions[$i] } @$values) {
			push @s, ucfirst(lc(substr($positions[$i], 0, 2)));
		} else {
			push @s, "--";
		}
	}
	return join('', @s);
}

sub house_to_str
{
	my ($house) = @_;
	my $s = "House $house->{number}:";
	$s .= " Na:" . possible_values_to_str($house->{name}, @NAMES);
	$s .= " Dr:" . possible_values_to_str($house->{drink}, @DRINKS);
	$s .= " Tr:" . possible_values_to_str($house->{transport}, @TRANSPORTS);
	$s .= " Co:" . possible_values_to_str($house->{colour}, @COLOURS);
	$s .= " Pe:" . possible_values_to_str($house->{pet}, @PETS);
	return $s;
}

sub houses_to_str
{
	my $s = "";
	$s .= "   " . house_to_str($_) . "\n" foreach @houses;
	return $s;
}


# Do one iteration of applying all the rules.
sub apply_rules
{
	# Remember the current state by just stringifying everything.
	my $currentstate = houses_to_str();
	
	while (my ($rulename, $rulecode) = each %RULES) {
		foreach my $house (@houses) {
			&$rulecode($house);
		}
		further_deduction();
		# Did that rule change anything? If so, show us:-
		my $newstate = houses_to_str();
		if ($newstate ne $currentstate) {
			print "Rule applied: $rulename\n";
			$currentstate = $newstate;
			print $currentstate;
		}
	}
}


# After applying rules, we may (possibly) be able to identify a situation where
# we can cross out more things. This rougly corresponds to the rule "there is
# only one of each of the different kinds of things".
sub further_deduction
{
	foreach my $category (qw/name drink transport colour pet/) {
		foreach my $house (@houses) {
			# If a given category has exactly one possible value, we are certain about it, and can exclude all those values
			# within the same category from other houses.
			if (@{$house->{$category}} == 1) {
				my $known = $house->{$category}->[0];
				foreach my $otherhouse (@houses) {
					next if $otherhouse->{number} == $house->{number};
					filter { $_ ne $known } $otherhouse->{$category};
				}
			}
			
			# Conversely, if a possible value of this category has been crossed out on all other houses, we can be certain
			# that it is the correct value for this house's category, and exclude others.
			foreach my $possible_value_for_this_house (@{$house->{$category}}) {
				# Let's map the list of houses into a list of all still-possible values for that category (e.g. all potential pets),
				# and grep that further by this one possible value we're currently considering.
				my @possible = grep { $_ eq $possible_value_for_this_house } map { @{$_->{$category}} } @houses;
				if (@possible == 1) {
					# Aha! It's this one.
					$house->{$category} = [ $possible_value_for_this_house ];
					last;
				}
			}
		}
	}
}


# Apply the rules until we reach a steady state.
sub apply_rules_repeatedly
{
	# Remember the current state by just stringifying everything.
	my $currentstate = "";
	while (1) {
		apply_rules();
		my $newstate = houses_to_str();
		last if $newstate eq $currentstate;
		$currentstate = $newstate;
	}
}


apply_rules_repeatedly();

