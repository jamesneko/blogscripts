#!/usr/bin/perl -CSDA

use strict;
use warnings;
use utf8;
use 5.014;
use Encode qw/encode_utf8 decode_utf8/;
use Getopt::Long;
use File::Find;
use File::Basename;
use File::Spec;
use Cwd qw/cwd abs_path/;
use List::MoreUtils qw/firstidx/;
use Term::ANSIColor;

use Data::Dumper;

use JSON;	# libjson-perl

my %opts = ( 'ptrfile' => '.ptr' );
GetOptions(\%opts, "get", "set=s", "init", "match=s", "ignore=s", "increment", "decrement", "ptrfile=s", "list", "help");

my $called_as = basename($0);
$opts{get} = 1 if $called_as eq 'ptr' || $called_as eq 'ptr++' || $called_as eq 'ptr--';
$opts{increment} = 1 if $called_as eq 'ptr++';
$opts{decrement} = 1 if $called_as eq 'ptr--';

my $searchpath = $ARGV[0] // cwd();
$searchpath = abs_path($searchpath);

sub help
{
	my ($error) = @_;
	my $cmd = basename($0);
	my $help = <<EOF;
		Usage:
			$cmd --init : Drops a $opts{ptrfile} file at your current directory, pointing to the first file it can find in the tree.
			     Can also specify a regex pattern to --ignore and/or --match.
			     Ignore defaults to '^\\.' to ignore dotfiles and an example for match would be '\\.(mp4|mkv)\$'.
			$cmd --get  : Returns the currently pointed-to file.
			$cmd --set <filename> : Assigns the currently pointed-to file.
			$cmd --increment : Changes the currently pointed-to file to be the next one in the sequence.
			$cmd --decrement : Changes the currently pointed-to file to be the previous one in the sequence.
			$cmd --list : Just show what files are visible to the $opts{ptrfile} file.
		
		If called as `ptr`, will do a --get.
		If called as `ptr++`, will do a --get, and then --increment.
		If called as `ptr--`, will do a --get, and then --decrement.
		Note that for filenames with spaces, you may have to put it in your command line as "\$(ptr++)" instead.
		
		You can supply a directory as an argument to use it as the starting point for finding the $opts{ptrfile} file, rather than using your current directory.
EOF
	$help =~ s/^\t\t//gm;
	$help =~ s/\t/   /g;
	say STDERR colored($error, 'bold red') if $error;
	say STDERR $help;
	exit 1;
}


sub main
{
	say STDERR colored("Search path: $searchpath", 'white');

	# First, we need to find (or make!) our .ptr file.
	my $ptrfile;
	if ($opts{init}) {
		$ptrfile = $searchpath . "/" . $opts{ptrfile};
		init($ptrfile);
	} else {
		$ptrfile = find_ptrfile($searchpath);
		help("Unable to find $opts{ptrfile} file; Please use --init to create one or change the search path.") unless ($ptrfile);
	}
	say STDERR colored("Ptrfile: $ptrfile", 'white');
	
	# Now process any options relevant to getting/setting the pointer.
	if ($opts{get}) {
		my $ptr = get($ptrfile);
		say STDERR colored("Current pointer value: $ptr->{file}", 'white');
		# Report pointer value to programs as abs path, since we store them relative to .ptr file, and the user may not be in that dir.
		say File::Spec->rel2abs($ptr->{file}, dirname($ptrfile));
	}
	if ($opts{list}) {
		my $ptr = get($ptrfile);
		my @files = file_list(dirname($ptrfile), $ptr);
		foreach my $file (@files) {
			say $file;
		}
	}
	if ($opts{set}) {
		my $ptr = get($ptrfile);
		# If the argument to --set is already relative, it is first converted to absolute based on the current working dir,
		# which (if we haven't called file_list() yet) should be what the user thinks it is. Then we convert it back to a
		# relative path from wherever the .ptr file is kept.
		my $relpath = File::Spec->abs2rel($opts{set}, dirname($ptrfile));
		$ptr->{file} = $relpath;
		set($ptrfile, $ptr);
		say STDERR colored("Set pointer to $ptr->{file}", 'white');
	}
	if ($opts{increment}) {
		my $ptr = get($ptrfile);
		my @files = file_list(dirname($ptrfile), $ptr);
		crement($ptr, +1, @files);
		set($ptrfile, $ptr);
		say STDERR colored("Incremented pointer to $ptr->{file}", 'white');
	}
	if ($opts{decrement}) {
		my $ptr = get($ptrfile);
		my @files = file_list(dirname($ptrfile), $ptr);
		crement($ptr, -1, @files);
		set($ptrfile, $ptr);
		say STDERR colored("Decremented pointer to $ptr->{file}", 'white');
	}
}


sub get
{
	my ($ptrfile) = @_;
	# Then we need to slurp it in.
	my $json_text = slurp_file($ptrfile);
	# Then interpret it as JSON, and return the hash.
	return JSON->new->decode($json_text);
}


sub set
{
	my ($ptrfile, $json) = @_;
	# Format it back to text.
	my $json_text = JSON->new->pretty->encode($json);
	# Spew it to file.
	spew_file($ptrfile, $json_text);
	return $json;
}


sub init
{
	my ($ptrfile) = @_;
	# Basic structure, minus first file.
	my $json = {
			ignore => $opts{ignore} // '^\.',
			match => $opts{match} // '.*',
		};
	# Set the pointer to be the first file in the list.
	my @files = file_list(dirname($ptrfile), $json);
	$json->{file} = $files[0] // "";
	set($ptrfile, $json);
}


sub crement
{
	my ($ptr, $mod, @files) = @_;
	my $idx = firstidx { $_ eq $ptr->{file} } @files;	# Our currently pointed-to file SHOULD be in the files list.
	$idx = ($idx + $mod) % @files unless @files == 0;
	$ptr->{file} = $files[$idx] // "";
}


# Iterate upwards through directories until we find a file called .ptr
sub find_ptrfile
{
	my ($dirname) = @_;
	# This step will probably not work on legacy OSes with volume identifiers in the paths.
	# I'm not coding around all that C: D: cruft. Use cygwin or msys or whatever to fix it maybe.
	# In fact, I don't even wanna consider a path separator that isn't '/'.
	if (-f $dirname . "/" . $opts{ptrfile}) {
		return $dirname . "/" . $opts{ptrfile};
	} else {
		my $up = dirname($dirname);	# returns all but last path entry even if last entry is clearly a dir with a trailing /
		return if $up eq $dirname;		# return undef if we can't find anything.
		return find_ptrfile($up);
	}
}


sub slurp_file
{
	my ($filename) = @_;

	open(my $fh, '<:utf8', $filename) or die "Couldn't read from '$filename' : $!\n";
	local $/ = undef;	# unset record separator to slurp entire file in one readline call.
	my $contents = readline $fh;
	close($fh) or die "Couldn't close file '$filename' : $!\n";
	
	return $contents;
}


sub spew_file
{
	my ($filename, $contents) = @_;
	
	open(my $fh, '>:utf8', $filename) or die "Couldn't write to '$filename' : $!\n";
	print $fh $contents or die "Couldn't write to '$filename' : $!\n";
	close($fh) or die "Couldn't close file '$filename' : $!\n";
}


sub file_list
{
	my ($absbasedir, $ptr) = @_;
	my @files;
	# File::Find works with binary strings from the filesystem only.
	# We need to convert our UTF-whatever flagged Perl strings into a utf8 encoded binary string
	# before passing them to find(), and decode the filenames we get from it.
	my $absbasedir_utf8 = $absbasedir;
	encode_utf8($absbasedir_utf8);
	find({
			wanted => sub {
				# Apparently File::Find doesn't handle utf8 filenames.
				my $currfilename = decode_utf8($_);
				my $fullpathname = decode_utf8($File::Find::name);
				my $basename = basename($fullpathname);
				return unless (-e $fullpathname && ! -d $fullpathname);
				return if $basename eq $opts{ptrfile};
				return if $basename =~ /$ptr->{ignore}/;		#### TODO: Can we ignore e.g. entire .hg/ dirs with this?
				return unless $basename =~ /$ptr->{match}/;
				
				# We run the path through abs2rel because we do the same conversion for --set, and if we want
				# increment to work properly we need to have the same format for relative paths; File::Find would
				# be returning stuff like "./dir1/file1", while abs2rel gives us "dir1/file1".
				push @files, File::Spec->abs2rel($fullpathname, $absbasedir);
			},
			follow_skip => 2, # If we get duplicate dirs, files etc. during find, ignore and carry on.
			no_chdir => 1,    # We want our current dir consistent after Find does its work.
		}, $absbasedir_utf8);
	return sort @files;
}


help() if $opts{help};
help() unless $opts{get} || $opts{set} || $opts{init} || $opts{increment} || $opts{decrement} || $opts{list};
main();
